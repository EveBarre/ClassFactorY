#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
@author : Ève Barré

--> General description :

The TF's selection is done on its coverage and specificity for a gene pattern. 
Coverage depict which part of the gene pattern is regulated by a specific TF. 
Specificity represents the number of targets of a TF which belong to a specific pattern i.e., 
if a TF preferentially regulates one gene expression pattern over the others. 
Select_tf produces specificity and coverage calculations. 
Once computed, percentages are used to compare TF and select major relations. 
A threshold can be set as mean + one standard deviation, a quantile and/or a specific percentage. 


--> Arguments : 

    "-i", "--input_file" : Path to the file containing relations' information (must have as columns labels : 'Transcription_Factor', 'Transcription_Factor', 'Gene', 'Pattern_Gene', 'Pattern_TF', 'Reg') and a .csv file.
    "-rel", "--store_relation_table_file" : Path to the selected reltations' table file, if wanted to be returned (.csv).
    "-ttf", "--table_TF" : Path to the selected transcription factors file, if wanted to be returned (.csv).
    "-tcg", "--table_count_gene_by_pattern" : Table of number of gene by pattern on all relations (.csv).
    "-th", "--threshold_type" : The type of threshold can be : mean+std, q50, q75.
    "-percs", "--percentage_threshold_cov_spe" : Minimum percentage of TF's coverage and specificy for a gene_pattern in decimal to filter, example : '0.75,0.80', '0.75,No' <=> 'cov,spe'.
    "-presel", "--preselected_table" : Path to  the preselected table but with data on the selection (.csv).
    "-col", "--columns_TF" : Specifies columns in the output table to concerve (example for all columns : 'Transcription_Factor', 'Pattern_TF', 'Nb_genes_per_TF', 'Nb_relations_selected', 'selected_pattern_nb', 'Patterns_targeted_pos', 'Patterns_targeted_neg', 'Patterns_targeted'.).
    "--follow_process_path" : Path to the file to save process data (markdown file .md).
    "-s", "--sep_table" : String that separates columns in tables, example : '\t', ',', ....
    # "--are_patterns_given" : Bool, if patterns' columns for transcripton facotr and gene are given.
    # "--is_regulatory_direction_given" : Bool, if regylatory direction between transcription factor and gene are given.


--> Proceeding : 

    - writes (and stores in a follow_process file) process' data and general data informaitons all along the script :
        - script name and time/date
        - arguments used
        - relations number in along filter
        - Statistics on the number of genes by genes' pattern
        - Statistics on the number of genes by TF
        - calculations' durations
        - constant patterns selected count 
        - TF's pattern per gene pattern selected
        - Transcription factors per TF's pattern selected
        - Selected transcription factors number

    - (i) imports relations table, drop duplicates 
    - prepares specificity and coverages calculations : gene per gene pattern count (tcg), gene per TF count, relation TF_gene count.
    - calculates specificity (relation TF_gene count / gene per TF count) and coverage (relation TF_gene count / gene per gene pattern count) 
    - (presel) stores preselected with spe/cov data in a file 
    - select spe and cov in function of methode chosen : 
        - (-th mean+std) mean and standard derivation  
        - (-th q75) quantile (q75, q85, etc...)
        - (-percs) a percentage threshold 
    - (-rel) stores selected relations in a file 
    - (-col) select columns to conserve in the transcription factors file
        - Transcription_Factor
        - Pattern_TF
        - Nb_genes_per_TF
        - Nb_relations_selected
        - selected_pattern_nb
        - Patterns_targeted_pos
        - Patterns_targeted_neg
        - (add mean spe.cov per TF...)
    - (-ttf) stores TF table 

--> In pipeline : 

Select_tf is in the center of the pipeline. 
The query is prepared by group_pattern and get_annot_tf to use next allow to continue 
TF classification.

--> Usage tips : 

Storing new relation table can be useful to research a specific interation between a TF and a gene for instance. 
It is recommanded by default. 
To have the TF table at the end can be a good sum up to relations selected. 
To keep preselected table with coverage and specificity columns can check parameters for a TF of interest 
or instance, if it are not passed filter.
To keep 'Patterns_targeted' columns is required for the module (graphs).. use. 

--> Execution time and memory usage : 

'''

import pandas as pd
import argparse 
import time 
import datetime 
import os
import matplotlib.pyplot as plt
from resource import getrusage, RUSAGE_SELF  # to assess the peak memory usage
import pipeline

# functions needed

# Function to follow the process and prints :
def pre_threshold(Relation_table, preselected_table, sep_table) -> float:
    '''
    '''
    # Asserts on values : 

    if any(Relation_table["Specificity"] > 1) :
        raise ValueError("There are specificities > 0.")

    if any(Relation_table["Coverage"] > 1) :
        raise ValueError("There are coverages > 0.")


    # Selects relations that pass the filter : 

    # To have the table of specificities and coverages preselected :

    if preselected_table != None :
        if not os.path.exists("/".join(preselected_table.split("/")[:-1])):
            os.makedirs("/".join(preselected_table.split("/")[:-1]))
        Relation_table.to_csv(preselected_table, sep = sep_table, index = None)

    print("Selects unique TF_Gene relations that pass the threshold.")
    print(f"The table to process has {Relation_table.shape[0]} lines.")

    return time.time()


def select_tf(follow_process_path, input_file, store_relation_table_file, table_TF, table_count_gene_by_pattern, threshold_type, percentage_threshold_cov_spe, preselected_table, columns_TF, sep_table, gene_patterns_to_delete, TF_patterns_to_delete) :  # are_patterns_given, is_regulatory_direction_given, 
    '''
    '''

    t0 = time.time()

    if (not store_relation_table_file) & (not preselected_table) & (not table_TF) : 
        raise ValueError(f"At least one of preselected table, relations table and TF table must be stored.")

    # asserts is a .md file 
    if follow_process_path[-3:] != ".md" :
        raise ValueError(f"'follow_process_path' must be a markdown (.md) file not {follow_process_path.split('.')[-1]}.")

    # Checks input files' existance :
    if not os.path.exists(input_file):
        raise ValueError(f"{input_file} does not exist. Change input.")

    if not os.path.exists(table_count_gene_by_pattern):
        raise ValueError(f"{table_count_gene_by_pattern} does not exist. Change input.")

    if not os.path.exists("/".join(follow_process_path.split("/")[:-1])):
        os.makedirs("/".join(follow_process_path.split("/")[:-1]))

    follow_process_dir = "/".join(follow_process_path.split("/")[:-1])

    # Checks if arguments are missing : 
    if not input_file : 
        raise ValueError("Input file missing. (--input_file)")

    if percentage_threshold_cov_spe != None : 
        if (percentage_threshold_cov_spe.count(",") != 1) : 
            raise ValueError(f"Coverage and specificity percentages threshold must be written as : cov, spe with one comma but not {percentage_threshold_cov_spe.count(',')}")
        [cov, spe] = percentage_threshold_cov_spe.split(", ")
        if cov != "No" : 
            if float(cov) > 1 : 
                raise ValueError(f"Coverage percentage threshold must be a float. As 0.5 for 50%, but not {cov}.")
        if spe != "No" : 
            if float(spe) > 1 : 
                raise ValueError(f"Specificity percentage threshold must be a float. As 0.5 for 50%, but not {spe}.")
    
    if not table_count_gene_by_pattern : 
        raise ValueError("Gene by pattern count's table file missing. (--table_count_gene_by_pattern)")
    
    # markdown file to follow process :
    text_info = (f"""

# <font color=orange>Module select_tf.py :</font> 

lauched at {datetime.datetime.today()}

with arguments : 

| Arguments | |
| --- | --- |
| store_relation_table_file | {store_relation_table_file} |
| table_TF | {table_TF} |
| table_count_gene_by_pattern | {table_count_gene_by_pattern} |
| follow_process_path | {follow_process_path} |
| input_file | {input_file} |
| threshold_type | {threshold_type} |
| sep_table | {sep_table} |
| percentage_threshold_cov_spe | {percentage_threshold_cov_spe} |
| columns_TF | {columns_TF} |
| preselected_table | {preselected_table} |
| gene_patterns_to_delete | {gene_patterns_to_delete} |
| TF_patterns_to_delete | {TF_patterns_to_delete} |

""")
# | is_regulatory_direction_given | {is_regulatory_direction_given} |
# | are_patterns_given | {are_patterns_given} |
    print(f"""
********************************************************************************
--> Module select_tf.py :
lauched at {datetime.datetime.today()}
********************************************************************************
""")

    with open(follow_process_path, "a") as follow_process_file :
        follow_process_file.write(text_info)

    input_size = os.path.getsize(input_file) + os.path.getsize(table_count_gene_by_pattern)

    # Importation of relations' table : 
    Relation_table = pd.read_csv(input_file, sep = sep_table, header = 0, usecols = ["Gene", "Pattern_Gene", "Transcription_Factor", "Pattern_TF", "Reg"], engine = 'python').drop_duplicates()

    Relation_table["Pattern_Gene"] = Relation_table["Pattern_Gene"].astype(float).astype("Int64").astype(str)
    Relation_table["Pattern_TF"] = Relation_table["Pattern_TF"].astype(float).astype("Int64").astype(str)
    len_pattern = len(str(list(Relation_table["Pattern_Gene"])[0]))
    if len_pattern != len(str(list(Relation_table["Pattern_TF"])[0])) :
        raise ValueError(f"Patterns must have same length (TF {len(str(list(Relation_table['Pattern_TF'])[0]))} and genes {len_pattern}).")

    # deletes gene's pattern unwanted : 
    if gene_patterns_to_delete :
        for gene_pattern in gene_patterns_to_delete.split(', ') :
            Relation_table = Relation_table[Relation_table["Pattern_Gene"] != gene_pattern]

    # deletes TF's pattern unwanted : 
    if TF_patterns_to_delete :
        for TF_pattern in TF_patterns_to_delete.split(', ') :
            Relation_table = Relation_table[Relation_table["Pattern_TF"] != TF_pattern]

    # Concerves the information on duplicate to filter TF_Gene relation ? 

    # Tables that helpes to specificity and coverage calculation (divisors)
    # Number of gene per gene's pattern : 

    Count_divis_cov = pd.read_csv(table_count_gene_by_pattern, sep = sep_table, header = 0, usecols = ["Pattern_Gene", "Nb_genes_per_gene_pattern"], engine = 'python')
    # Count_divis_cov = pd.read_csv(table_count_gene_by_pattern, sep = sep_table, header = None, names = ["Pattern_Gene", "Nb_genes_per_gene_pattern"]) 
    Count_divis_cov["Pattern_Gene"] = Count_divis_cov["Pattern_Gene"].astype(float).astype("Int64").astype(str)
    Relation_table = Relation_table.merge(Count_divis_cov, on = ["Pattern_Gene"], how = "left")

    Count_divis_spe = Relation_table[["Transcription_Factor", "Gene"]].groupby("Transcription_Factor")["Gene"].nunique().rename("Nb_genes_per_TF")  # Number of gene per TF
    Relation_table = Relation_table.merge(Count_divis_spe, on = ["Transcription_Factor"], how = "left")

    Count_rel_TF_Gene = Relation_table[["Transcription_Factor", "Gene", "Pattern_Gene"]].groupby(["Transcription_Factor", "Pattern_Gene"])["Gene"].nunique().rename("Nb_rel_TF_Gene")
    Relation_table = Relation_table.merge(Count_rel_TF_Gene, on = ["Transcription_Factor", "Pattern_Gene"], how = "left") 

    # To store process' informations : 
    pipeline.stat(Relation_table[["Nb_genes_per_gene_pattern"]], "Nb_genes_per_gene_pattern", "hist, box", "\t", f"{follow_process_dir}/graphics")

    text_info = f"""


# Data analysis and statistics


## Statistics on the number of genes by genes' pattern : 
***

{pipeline.display_table_stat("graphics/Nb_genes_per_gene_pattern_hist.png", "Nb_genes_per_gene_pattern_hist", "graphics/Nb_genes_per_gene_pattern_box.png" , "Nb_genes_per_gene_pattern_box", Relation_table[["Nb_genes_per_gene_pattern"]].describe().to_markdown())}

***
""" 
    pipeline.stat(Relation_table[["Nb_genes_per_TF"]], "Nb_genes_per_TF", "hist, box", "\t", f"{follow_process_dir}/graphics")

    text_info += f"""


## Statistics on the number of genes by TF  : 
***

{pipeline.display_table_stat("graphics/Nb_genes_per_TF_hist.png", "Nb_genes_per_TF_hist", "graphics/Nb_genes_per_TF_box.png" , "Nb_genes_per_TF_box", Relation_table[["Nb_genes_per_TF"]].describe().to_markdown())}

***
""" 
    with open(follow_process_path, "a") as info_file:
        info_file.write(text_info)

    # Asserts : 

    if any(Relation_table["Nb_rel_TF_Gene"] > Relation_table["Nb_genes_per_TF"]) : 
        raise ValueError("Some Nb_genes_per_TF are < Nb_rel_TF_Gene.")

    if any(Relation_table["Nb_rel_TF_Gene"] > Relation_table["Nb_genes_per_gene_pattern"]) : 
        raise ValueError("Some Nb_genes_per_gene_pattern < Nb_rel_TF_Gene.")


    # Calculation of each TF-Gene's relation's specificity and coverage :

    print("Calculates specificities.")
    print(f"The table to calculate has {Relation_table.shape[0]} lines.")
    t0 = time.time()

    Relation_table["Specificity"] = Relation_table["Nb_rel_TF_Gene"] / Relation_table["Nb_genes_per_TF"] 

    t1 = time.time()
    t_fin = round((t1 - t0), 3)
    print(f"Calulation of specificity's table done in {t_fin} seconds\n.") 

    # To store process time : 
    with open(follow_process_path, "a") as info_file :
        info_file.write("# Execution informations (time, memory):\n\n")
        info_file.write(f"Calulation of specificity's table done in **{t_fin} seconds**.\n\n") 


    print("Calculates coverages.")
    print(f"The table to process has {Relation_table.shape[0]} lines.")
    t0 = time.time()

    Relation_table["Coverage"] = Relation_table["Nb_rel_TF_Gene"] / Relation_table["Nb_genes_per_gene_pattern"] 

    t1 = time.time()
    t_fin = round((t1 - t0), 3)
    print(f"Calulation of coverage's table done in {t_fin} seconds.\n") 

    # To stores process time : 
    with open(follow_process_path, "a") as info_file:
        info_file.write(f"Calulation of coverage's table done in **{t_fin} seconds.**\n\n") 

    # Calculates specificitis and coverages thresholds per gene's pattern : 
    if threshold_type != None :
        if threshold_type == "mean+std" :

            # Specificities :
            Avg_spe_table = Relation_table[["Transcription_Factor", "Specificity", "Pattern_Gene"]].drop_duplicates().groupby(["Pattern_Gene"])["Specificity"].mean().rename( "Spe_mean")
            Relation_table = Relation_table.merge(Avg_spe_table, on = ["Pattern_Gene"], how = "left")

            Std_spe_table = Relation_table[["Transcription_Factor", "Specificity", "Pattern_Gene"]].drop_duplicates().groupby(["Pattern_Gene"])["Specificity"].std().rename("Spe_std")
            Relation_table = Relation_table.merge(Std_spe_table, on = ["Pattern_Gene"], how = "left")

            # Coverages :
            Avg_cov_table = Relation_table[["Transcription_Factor", "Pattern_Gene", "Coverage"]].drop_duplicates().groupby(["Pattern_Gene"])["Coverage"].mean().rename("Cov_mean")
            Relation_table = Relation_table.merge(Avg_cov_table, on = ["Pattern_Gene"], how = "left")

            Std_cov_table = Relation_table[["Transcription_Factor", "Pattern_Gene", "Coverage"]].drop_duplicates().groupby(["Pattern_Gene"])["Coverage"].std().rename("Cov_std")
            Relation_table = Relation_table.merge(Std_cov_table, on = ["Pattern_Gene"], how = "left")

            t0 = pre_threshold(Relation_table, preselected_table, sep_table)

            if (not store_relation_table_file) & (not table_TF) : 
                return [round((time.time() - t0), 3), round(((time.time() - t0)/60), 3), int(getrusage(RUSAGE_SELF).ru_maxrss / 1024), int(input_size / 1024)]
            
            Relation_table = Relation_table[(Relation_table["Specificity"] > (Relation_table["Spe_mean"] + Relation_table["Spe_std"])) & (Relation_table["Coverage"] > (Relation_table["Cov_mean"] + Relation_table["Cov_std"]))]

            if Relation_table.shape[0] == 0 : 
                print(f"""

*********** No one transcription factors are selected with the input threshold method. ***********

""")
                return [round((time.time() - t0), 3), round(((time.time() - t0)/60), 3), int(getrusage(RUSAGE_SELF).ru_maxrss / 1024), int(input_size / 1024)]


        elif threshold_type[0] == "q" :
            quartile = threshold_type[1:]   

            # Specificity :
            q_spe_table = Relation_table[["Transcription_Factor", "Specificity", "Pattern_Gene"]].drop_duplicates().groupby(["Pattern_Gene"])["Specificity"].quantile(float("0." + quartile)).rename("Spe_q" + quartile)
            Relation_table = Relation_table.merge(q_spe_table, on = ["Pattern_Gene"], how = "left")
           
            # Coverage :
            q_cov_table = Relation_table[["Transcription_Factor", "Pattern_Gene", "Coverage"]].drop_duplicates().groupby(["Pattern_Gene"])["Coverage"].quantile(float("0." + quartile)).rename("Cov_q" + quartile)
            Relation_table = Relation_table.merge(q_cov_table, on = ["Pattern_Gene"], how = "left")

            t0 = pre_threshold(Relation_table, preselected_table, sep_table)
            if (not store_relation_table_file) & (not table_TF) : 
                return [round((time.time() - t0), 3), round(((time.time() - t0)/60), 3), int(getrusage(RUSAGE_SELF).ru_maxrss / 1024), int(input_size / 1024)]

            Relation_table = Relation_table[(Relation_table["Specificity"] > Relation_table["Spe_q" + quartile]) & (Relation_table["Coverage"] > Relation_table["Cov_q" + quartile])]

            if Relation_table.shape[0] == 0 : 
                print(f"""

*********** No one transcription factors are selected with the input threshold method. ***********

""")
                return [round((time.time() - t0), 3), round(((time.time() - t0)/60), 3), int(getrusage(RUSAGE_SELF).ru_maxrss / 1024), int(input_size / 1024)]

        else : 
            raise ValueError(f"Argument for the type of threshold {threshold_type} is not correct, must be : 'mean+std' or a quantile (like : 'q85', 'q95'...).")


    if percentage_threshold_cov_spe != None : 
        # [cov, spe] = percentage_threshold_cov_spe.split(",")  # Done previously to assert that are float 
        if cov != "No" :
            Relation_table = Relation_table[Relation_table["Coverage"] > float(cov)]
            if Relation_table.shape[0] == 0 : 
                print(f"""

*********** No one transcription factors are selected with the input threshold method. ***********

""")
                return [round((time.time() - t0), 3), round(((time.time() - t0)/60), 3), int(getrusage(RUSAGE_SELF).ru_maxrss / 1024), int(input_size / 1024)]
        
        if spe != "No" :
            Relation_table = Relation_table[Relation_table["Specificity"] > float(spe)]
            if Relation_table.shape[0] == 0 : 
                print(f"""

*********** No one transcription factors are selected with the input threshold method. ***********

""")
                return [round((time.time() - t0), 3), round(((time.time() - t0)/60), 3), int(getrusage(RUSAGE_SELF).ru_maxrss / 1024), int(input_size / 1024)]

    t1 = time.time()
    t_fin = round((t1 - t0), 3)
    print(f"Table's filter done in {t_fin} seconds.\n") 

    # To stores process time : 
    with open(follow_process_path, "a") as info_file:
        info_file.write(f"Selection of table done in **{t_fin} seconds**.\n\n")
    
    with open(follow_process_path, "a") as info_file:
        info_file.write("# Supplemental informations :\n")

        info_file.write("\nTF's pattern per gene pattern selected :\n\n\n")
        Relation_table["Pattern_Gene"] = Relation_table["Pattern_Gene"].astype(str)
        Relation_table["Pattern_TF"] = Relation_table["Pattern_TF"].astype(str)
        TF_pattern_per_gene_pattern_table = Relation_table[["Pattern_Gene", "Pattern_TF"]].drop_duplicates().groupby(["Pattern_Gene"])["Pattern_TF"].agg(", ".join).reset_index(name = "TFs_pattern")
        TF_pattern_per_gene_pattern_table.to_markdown(info_file)

        info_file.write("\n\nTranscription factors per TF's pattern selected :\n\n\n")
        TF_per_TF_pattern_table = Relation_table[["Pattern_TF", "Transcription_Factor"]].drop_duplicates().groupby(["Pattern_TF"])["Transcription_Factor"].agg(", ".join).reset_index(name = "TFs")
        TF_per_TF_pattern_table.to_markdown(info_file)

    if store_relation_table_file :
        if not os.path.exists("/".join(store_relation_table_file.split("/")[:-1])):
            os.makedirs("/".join(store_relation_table_file.split("/")[:-1]))
        Relation_table.to_csv(store_relation_table_file, sep = sep_table, index = None)

    
    list_TF = Relation_table[["Transcription_Factor", "Pattern_TF", "Gene", "Nb_genes_per_TF"]].groupby(["Transcription_Factor", "Pattern_TF", "Nb_genes_per_TF"]).Gene.count().reset_index(name = "Nb_relations_selected")
    # adds the number of genes targeted by the TF
    # with or without genes
    # list_TF["Genes_targeted"] = Relation_table[["Transcription_Factor", "Pattern_TF", "Gene"]].groupby(["Transcription_Factor", "Pattern_TF"])["Gene"].apply(",".join).reset_index(name = "Genes_targets")["Genes_targets"]

    pattern_selected_nb = Relation_table[["Transcription_Factor", "Pattern_Gene"]].drop_duplicates().groupby(["Transcription_Factor"]).Pattern_Gene.count().reset_index(name = "selected_pattern_nb")
    list_TF = list_TF.merge(pattern_selected_nb, how = "left")
    pattern_selected_nb = []
    Relation_table["Pattern_Gene"] = Relation_table["Pattern_Gene"].astype(str)

    # list_TF["Patterns_targeted"] = Relation_table[["Transcription_Factor", "Pattern_Gene"]].drop_duplicates().groupby(["Transcription_Factor"])["Pattern_Gene"].agg(",".join).reset_index(name = "Patterns_targets")["Patterns_targets"]
    
    # list_TF["Reg_patterns_targeted"] = Relation_table[["Transcription_Factor", "Pattern_Gene", "Reg"]].drop_duplicates().groupby(["Transcription_Factor"])["Reg"].agg(",".join).reset_index(name = "Patterns_targeted_reg")["Patterns_targeted_reg"]
    Patterns_targeted_pos = Relation_table[["Transcription_Factor", "Pattern_Gene", "Reg"]].drop_duplicates()
    Patterns_targeted_neg = Patterns_targeted_pos
    Patterns_targeted_pos = Patterns_targeted_pos[Patterns_targeted_pos["Reg"] == "+"].groupby(["Transcription_Factor"])["Pattern_Gene"].agg(", ".join).reset_index(name = "Patterns_targeted_pos")
    Patterns_targeted_neg = Patterns_targeted_neg[Patterns_targeted_neg["Reg"] == "-"].groupby(["Transcription_Factor"])["Pattern_Gene"].agg(", ".join).reset_index(name = "Patterns_targeted_neg")
    Patterns_targeted = Relation_table[["Transcription_Factor", "Pattern_Gene", "Reg"]].drop_duplicates().groupby(["Transcription_Factor"])["Pattern_Gene"].agg(", ".join).reset_index(name = "Patterns_targeted")    
    
    list_TF = list_TF.merge(Patterns_targeted_pos, how = "left", on = ["Transcription_Factor"])
    list_TF = list_TF.merge(Patterns_targeted_neg, how = "left", on = ["Transcription_Factor"])
    list_TF = list_TF.merge(Patterns_targeted, how = "left", on = ["Transcription_Factor"])


    # else :  # If wants to have data on coverage and specificities ... 

    print(f"\n{list_TF.shape[0]} transcription factors are selected.\n")

    with open(follow_process_path, "a") as info_file:
        info_file.write(f"\n{list_TF.shape[0]} transcription factors are selected.\n\n")
        info_file.write("\n***\n\n")

        # Was in the get_annot_tf script before, but must be better here 
        # Calculates TF's number for each distinct TF's profile and its count.
        info_file.write("\n## Statistics on TFs selected's Patterns :\n")

        df_count = list_TF[["Pattern_TF", "Transcription_Factor"]].groupby(["Pattern_TF"])["Transcription_Factor"].nunique().rename("nb_TF_per_pattern").to_frame().reset_index()
        df_count["Pattern_TF"] = df_count["Pattern_TF"].astype(str)
        df_write = df_count[["nb_TF_per_pattern", "Pattern_TF"]].groupby(["nb_TF_per_pattern"])["Pattern_TF"].apply(lambda x : ", ".join(x)).rename("TF_patterns").reset_index() 
    
        info_file.write("Number of TF's patterns per number of TF per TF's patterns : \n")
        df_write.to_markdown(info_file)

        info_file.write("\n\n")


    if table_TF != None: 
        if not os.path.exists("/".join(table_TF.split("/")[:-1])):
            os.makedirs("/".join(table_TF.split("/")[:-1]))
        if columns_TF : 
            list_TF[columns_TF.split(", ")].to_csv(table_TF, sep = sep_table, index = None)
        else : 
            list_TF.to_csv(table_TF, sep = sep_table, index = None)

        # Execution time 
    print(f"Ends at {round((time.time() - t0), 3)} seconds <=> {round(((time.time() - t0)/60), 3)} minutes")
    text_info = f"\nExecution time : {round((time.time() - t0), 3)} seconds <=> {round(((time.time() - t0)/60), 3)} minutes"

    with open(follow_process_path, "a") as follow_process_file :
        follow_process_file.write(text_info)
    
    # execution_time (sec), execution_time (min), memory_usage (MiB), input_file_size (MiB)
    return [round((time.time() - t0), 3), round(((time.time() - t0)/60), 3), int(getrusage(RUSAGE_SELF).ru_maxrss / 1024), int(input_size / 1024)]


if __name__ == "__main__" : 

    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input_file", help = "Path to the file containing relations' information." )
    parser.add_argument("-rel", "--store_relation_table_file", help = "Path to the selected reltations' table file, if wanted to be returned." )
    parser.add_argument("-ttf", "--table_TF", help = "Path to the selected transcription factors file, if wanted to be returned." )
    parser.add_argument("-tcg", "--table_count_gene_by_pattern", help = "Table of number of gene by pattern on all relations.")
    parser.add_argument("-th", "--threshold_type", help = "The type of threshold can be : mean+std, q50, q75, by default 'mean+std'") 
    parser.add_argument("-percs", "--percentage_threshold_cov_spe", help= "Minimum percentage of TF's coverage and specificy for a gene_pattern in decimal to filter, example : '0.75,0.80', '0.75,No' <=> 'cov,spe'.")
    parser.add_argument("-presel", "--preselected_table", help= "Path to  the preselected table but with data on the selection")
    parser.add_argument("-col", "--columns_TF", help = "Specifies columns in the output table to concerve, example (by default): '['Transcription_Factor', 'Pattern_TF', 'Nb_genes_selected', 'Nb_genes_per_TF']'.")
    parser.add_argument("--follow_process_path", help = "Path to the file to save process data.")
    parser.add_argument("-s", "--sep_table", help = "String that separates columns in tables.")
    parser.add_argument("--gene_patterns_to_delete", help = "Uninteresting genes' patterns to delete (as constant patterns).")
    parser.add_argument("--TF_patterns_to_delete", help = "Uninteresting TF's patterns to delete (as constant patterns).")
    # parser.add_argument("--are_patterns_given", help = "Bool, if patterns' columns for transcripton facotr and gene are given.")
    # parser.add_argument("--is_regulatory_direction_given", help = "Bool, if regylatory direction between transcription factor and gene are given.") 

    args = parser.parse_args()

    select_tf(args.follow_process_path, args.input_file, args.store_relation_table_file, args.table_TF, args.table_count_gene_by_pattern, args.threshold_type, args.percentage_threshold_cov_spe, args.preselected_table, args.columns_TF, args.sep_table, args.gene_patterns_to_delete, args.TF_patterns_to_delete)  # args.are_patterns_given, args.is_regulatory_direction_given, 
