#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
@author : Ève Barré

--> General description : 

This scripts provides two csv files (regulators +/-and targets) of relations concerning 
a gene of interest (That can be a transcription factor or not) among a csv file 
containing relations' informations that bust be like : 

col0 : Gene
col1 : Pattern_Gene
col2 : ID_ATAC
col3 : Pattern_ATAC
col4 : Transcription_Factor
col5 : Pattern_TF
col6 : Reg

separated by "\t" and with a header at line 0

--> Arguments :

    "-i", "--input_file" : Path to the file containing relations' information.
    "-g", "--Gene" : Gene symbole name of gene of interest.

--> Proceeding : 

    - imports all relations table
    - selects relations involving Gene (-g) in input as a Gene
    - stores transcription factors of this selected table as Gene in input regulators
    - gives the TFs' count
    - selects relations involving Gene (-g) in input as a transcription factor
    - stores Genes of this selected table as Gene (that is a transcription factor) in input targets
    - gives the Genes' count


--> The command line should be like :

"python get_rel_tf.py -i TF_Gene_allProfile_reg.csv -g TP63"

'''

# Importations and definition of arguments

import pandas as pd
import argparse 
import os
import time
import datetime
import matplotlib.pyplot
from resource import getrusage, RUSAGE_SELF  # to assess the peak memory usage

def get_rel_tf(follow_process_path, input_file, Gene, result_path, sep_table:str) :
    '''
    '''

    t0 = time.time()

    # asserts is a .md file 
    if follow_process_path[-3:] != ".md" :
        raise ValueError(f"'follow_process_path' must be a markdown (.md) file not {follow_process_path.split('.')[-1]}.")

    # markdown file to follow process :
    text_info = (f"""

# <font color=orange>Module get_rel_tf.py :</font> 

lauched at {datetime.datetime.today()}

with arguments : 

- input_file : {input_file}
- follow_process_path : {follow_process_path}
- Gene : {Gene}
- result_path : {result_path}
- sep_table : {sep_table}

""")
    print(f"""

--> Module get_rel_tf.py :
lauched at {datetime.datetime.today()}

""")

    # Checks existence of input files : 
    if not os.path.exists(input_file):
        raise ValueError(f"{input_file} does not exist. Change input.")

    if not os.path.exists("/".join(follow_process_path.split("/")[:-1])):
        os.makedirs("/".join(follow_process_path.split("/")[:-1]))

    General_table = pd.read_csv(input_file, sep = sep_table, header = 0)

    Gene_in = str(Gene)

    # table of related TF : 

    Regulators_table = General_table[General_table["Gene"] == Gene_in]

    if len(Regulators_table) != 0 :

        pattern_Gene = Regulators_table.iloc[2, 1]

        # Count the number of same relations TF-gene

        Regulators_table = Regulators_table.groupby(["Transcription_Factor", "Pattern_TF", "Reg"]).ID_ATAC.count()
        Regulators_table = Regulators_table.to_frame()
        Regulators_table = Regulators_table.rename(columns = {"ID_ATAC":"Number_regions"})

        with open(f"Regulators_{str(Gene)}.csv", "a") as file_reg :
            # Adds comments
            file_reg.write(f"# Gene (pattern = {pattern_Gene}) could be regulated by {Regulators_table.shape[0]} transcription factors.\n")
            file_reg.write("# Cell types' order in pattern : (NBC, MBC_igM, MBC_igG, PB).\n")
            
        Regulators_table.to_csv(f"Regulators_{str(Gene)}.csv", mode = "a", sep = sep_table

    else : 
        print("Your Gene is not in the input list. A result is not possible.")


    # table of related Genes :

    Targets_table = General_table[General_table["Transcription_Factor"] == Gene_in]

    if len(Targets_table) != 0 :

        pattern_TF = Targets_table.iloc[2, 5]

        # Count the number of same relations TF-gene

        Targets_table = Targets_table.groupby(["Gene", "Pattern_Gene", "Reg"]).ID_ATAC.count()
        Targets_table = Targets_table.to_frame()
        Targets_table = Targets_table.rename(columns = {"ID_ATAC":"Number_regions"})

    if not os.path.exists("/".join(result_path.split("/")[:-1])):
        os.makedirs("/".join(result_path.split("/")[:-1]))

        with open(result_path, "a") as file_gene :
            file_gene.write(f"# TF (pattern = {pattern_TF}) could regulate {Targets_table.shape[0]} Genes.\n")
            file_gene.write("# Cell types' order in pattern : (NBC, MBC_igM, MBC_igG, PB).\n")
            Targets_table.to_csv(file_gene, sep = sep_table, line_terminator = "\n")


    else : print("Your Gene in input is not considered as a transcription factor.")

    # Execution time 
    print(f"Ends at {round((time.time() - t0), 3)} seconds <=> {round(((time.time() - t0)/60), 3)} minutes")
    text_info += f"\nExecution time : {round((time.time() - t0), 3)} seconds <=> {round(((time.time() - t0)/60), 3)} minutes"

    with open(follow_process_path, "a") as follow_process_file :
        follow_process_file.write(text_info)
    
    # execution_time (sec), execution_time (min), memory_usage (MiB), input_file_size (MiB)
    return [round((time.time() - t0), 3), round(((time.time() - t0)/60), 3), int(getrusage(RUSAGE_SELF).ru_maxrss / 1024), int(os.path.getsize(input_file) / 1024)]

if __name__ == "__main__" : 

    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input_file", help = "Path to the file containing relations' information." )
    parser.add_argument("-g", "--Gene", help = "Gene symbole name of gene of interest." )
    parser.add_argument("--follow_process_path", help = "Path to the file to save process data.")
    parser.add_argument("--result_path", help = "Path to the file to save relations process selected.")

    args = parser.parse_args()

    get_rel_tf(args.follow_process_path, args.input_file, args.Gene, args.result_path, args.sep_table)