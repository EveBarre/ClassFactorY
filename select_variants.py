#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
@author : Ève Barré

--> General description : 
This module class variants if they are located on a transcription factor given binding sites (TFBS) on target gene's regulatory regions.
If data are not given, TFBS can be obtained on JASPAR REST API and gene coordinates in Ensembl REST API. 

VCF file must have #CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO first line. 

--> Arguments : 

  "--reference_genome" : Reference genome used and to use to search genome coordinates.
  "-s", "--sep_table" : String that separates columns in transcription table of interest.
  "--JASPAR_version" : If transcription factor binding sites must be search, precise which version of JASPAR database use (2020, 2018...)
  "--TF_gene_region_file" : Relations_table path, can contain or not regions coordinates.
  "--VCF_file" : Path to the Variant Calling Format file.
  "--output_variants_file" : Path to the file to output (with variants per transcription factor found).
  "--regulatory_gene_distance" : Distance between a gene and its regulatory regions to consider (ex : 500000 bases).
  "--are_regulatory_region_given" : Bool, if regions' genomic coordinates are already given in the relation table (start, end, chrom).
  "--TFBS_files" : Path to the transcription binding sites file if already search, otherwise, search on JASPAR.
  "--follow_process_path" : Path to the file to save process data.
  "--is_regulatory_direction_given" : Bool, if regulatory direction is given as a column in relations table.

--> Proceeding : 

- If regulatory region coordinates are not given (start, end, chrom) (--are_regulatory_region_given): 
    - searchs for each genes its genomes coordinates in ensembl database (for human genome) 
    through REST API for a reference genome given (--reference_genome)
    - defines regulatory regions at a given distance to genes location (--regulatory_gene_distance) 
- Selects variants (--VCF_file) that are in a regulatory region defined
- if TFBS are not given (TFBS_files), search bindings sites for each selected TF in JASPAR database 
through a REST API for a given version (--JASPAR_version) for the same reference genome
- Selects variants which are in a transcription factor binding site (TFBS): 
- Provide as output columns : 
    - 1) TF, 
    - 2) bases number of regulatory region of genes targeted by the TF, 
    - 3) number of variants in regulatory region of targeted genes, 
    - 4) mutational charge (3/2)
    - 5) Variants with its data
- writes in a file (follow_process_path) following informations  on query : 
    - Module name 
    - Date and time of launch
    - Arguments 

--> In pipeline : 

Select_variants is launch in the second part as a additional annotation. It can be used independently to other modules 
in order to prioritize variants but also with others to prioritize transcription factors. 

--> Usage tips : 

--> Performance : 

'''


import requests, sys
import ast 
import coreapi
import argparse
import pandas as pd
import time
import datetime
import io
import os
import matplotlib.pyplot as plt 
from resource import getrusage, RUSAGE_SELF  # to assess the peak memory usage
import ast
import pipeline

def read_vcf(path) :
  '''
  Function copied in <https://gist.github.com/dceoy/99d976a2c01e7f0ba1c813778f9db744>.
  '''
  with open(path, 'r') as f:
      lines = [l for l in f if not l.startswith('##')]
  return pd.read_csv(
      io.StringIO(''.join(lines)),
      dtype={'#CHROM': str, 'POS': int, 'ID': str, 'REF': str, 'ALT': str,
              'QUAL': str, 'FILTER': str, 'INFO': str},
      sep='\t'
  ).rename(columns={'#CHROM': 'CHROM'})

def get_region_loc_gene_grch37(Gene:str, regulatory_gene_distance:int, follow_process_path:str) :
  '''
  '''

  try : 

    print(f"Search regulatory region for {Gene}")
    
    server = "http://grch37.rest.ensembl.org"
    ext = f"/lookup/symbol/homo_sapiens/{Gene}?"

    r = requests.get(server + ext, headers = {"Content-Type" : "application/json"})

    if not r.ok:
      r.raise_for_status()
      sys.exit()

    decoded = r.json()
    gene_data = ast.literal_eval(repr(decoded))
    # keys : 'end', 'id', 'source', 'species', 'description', 'logic_name', 'version', 'object_type', 'assembly_name', 'biotype', 'seq_region_name', 'display_name', 'strand', 'start', 'db_type'
    start = int(gene_data["start"])
    end = int(gene_data["end"])

    if start > end : 
      raise ValueError(f"Sart must be smaller than End for gene location.")

  except : 
    print(f"Don't work for {Gene}")
    with open(follow_process_path, "a") as info_file:
      info_file.write(f"Can not find coordinated of {Gene}.")

    return {"start" : None, "end" : None, "CHROM" : None}

  return {"start" : (start - int(regulatory_gene_distance)), "end" : (end + int(regulatory_gene_distance)), "CHROM" : gene_data["seq_region_name"]}  # , "strand" : gene_data["strand"]}
 


def get_region_loc_gene_grch38(Gene:str, regulatory_gene_distance:int, follow_process_path:str) :
  '''
  '''

  try : 

    print(f"Search regulatory region for {Gene}")

    server = "http://rest.ensembl.org"
    ext = f"/lookup/symbol/homo_sapiens/{Gene}?"

    r = requests.get(server + ext, headers = {"Content-Type" : "application/json"})

    if not r.ok:
      r.raise_for_status()
      sys.exit()

    decoded = r.json()
    gene_data = ast.literal_eval(repr(decoded))
    start = gene_data["start"]
    end = gene_data["end"]

    if start > end : 
      raise ValueError(f"Sart must be smaller than End for gene location.")

  except : 
    print(f"Don't work for {Gene}")
    with open(follow_process_path, "a") as info_file:
      info_file.write(f"Can not find coordinated of {Gene}.")

    return {"start" : None, "end" : None, "CHROM" : None}

  return {"start" : (start - regulatory_gene_distance), "end" : (end + regulatory_gene_distance), "CHROM" : gene_data["seq_region_name"]}  # , "strand" : gene_data["strand"]}
 
 
def is_variant_in_TFBS(client, schema, TF:str, Genes_variants_table, JASPAR_version:str, reference_genome:str, sep_table:str, TFBS_table:str, follow_process_path:str) :
  '''
  '''
  if len(Genes_variants_table) == 0 : 
    return {"Transcription_Factor" : TF, "Variants_in_targeted_genes_regulatory_region_number" : len(Genes_variants_table),  "Variants_in_TFBS_nb" : 0, "Variants_in_TFBS" : "0"}

  if str(TFBS_table) == None : 
    
    # Search TFBS : 
    print(f"Search TFBS for {TF}.")
   
    # Interact with the API endpoint
    action = ["matrix", "list"]

    params = {
        "collection": 'CORE',
        "name": f'{TF}',
        "tax_group": 'vertebrates',
        "version": 'latest',
        "release": f'{JASPAR_version}',
    }
    result = client.action(schema, action, params=params)

    if len(result["results"]) ==  0 : 
      print(f"TFBS for {TF} not found.")
      with open(follow_process_path, "a") as info_file:
       info_file.write(f"TFBS for {TF} not found.")
      return {"Transcription_Factor" : TF, "Variants_in_targeted_genes_regulatory_region_number" : None, "Variants_in_TFBS_nb" : None, "Variants_in_TFBS" : None}

    matrix_id = result["results"][0]["matrix_id"]

    query_url = f"http://jaspar.genereg.net/api/v1/sites/{matrix_id}/"
    
    result = requests.get(query_url)
    
    if not result.ok :
      r.raise_for_status()
      sys.exit()
    
    decoded = result.json()
    result = []

    TFBS_table = pd.DataFrame.from_dict((ast.literal_eval(repr(decoded))["sites"]), orient = "columns")
    decoded = []

    if len(TFBS_table) == 0 : 
      print(f"TFBS for {TF} not found.")
      with open(follow_process_path, "a") as info_file:
       info_file.write(f"TFBS for {TF} not found.")
      return {"Transcription_Factor" : TF, "Variants_in_targeted_genes_regulatory_region_number" : None, "Variants_in_TFBS_nb" : None, "Variants_in_TFBS" : None}

    if TFBS_table["assembly"][0] != reference_genome :
      return {"Transcription_Factor" : TF, "Variants_in_targeted_genes_regulatory_region_number" : None, "Variants_in_TFBS_nb" : None, "Variants_in_TFBS" : None}

    TFBS_table = TFBS_table[["chrom", "start", "end"]]

    # Selects reference genome coordinates : 
    # TFBS_table = TFBS_table[TFBS_table["assembly"] == reference_genome]

  if len(TFBS_table) == 0 : 
    return {"Transcription_Factor" : TF, "Variants_in_targeted_genes_regulatory_region_number" : 0, "Variants_in_TFBS_nb" : str(len(TFBS_table)), "Variants_in_TFBS" : "0"}

  # Rename to merge on CHROM with variants 
  TFBS_table = TFBS_table.rename(columns = {"chrom" : "CHROM"})

  # if chromosome are not written just as a integer but with a string (to cope with XX et XY)
  i = 0
  while list(TFBS_table["CHROM"])[i].count("X") != 0 : 
    i += 1
  if not list(TFBS_table["CHROM"])[i].isdigit():
    TFBS_table["CHROM"] = TFBS_table["CHROM"].str.strip("".join(list(filter(lambda x: x.isalpha(), list(TFBS_table["CHROM"])[i]))))
    TFBS_table["CHROM"] = TFBS_table["CHROM"].astype(str)

  TFBS_table = TFBS_table.merge(Genes_variants_table, how = "inner")  # on CHROM normally 

  # Select variants that are in TFBS : 
  if len(TFBS_table) != 0 : 
    TFBS_table = TFBS_table[(TFBS_table["start"].astype(int) < TFBS_table["POS"].astype(int)) & (TFBS_table["POS"].astype(int) < TFBS_table["end"].astype(int))]

    if len(TFBS_table) != 0 : 
      # Gather all variants data in a unique column : 
      columns = list(TFBS_table.columns)
      columns.remove("Transcription_Factor")
      columns.remove("CHROM")
      columns.remove("start")
      columns.remove("end")
      TFBS_table["Variants"] = f"CHROM=" +TFBS_table["CHROM"].astype(str)
      for col_name in columns : 
        TFBS_table["Variants"] = TFBS_table["Variants"] + f";{col_name}=" + TFBS_table[col_name].astype(str)

      return {"Transcription_Factor" : TF, "Variants_in_targeted_genes_regulatory_region_number" : len(Genes_variants_table), "Variants_in_TFBS_nb" : str(len(TFBS_table)), "Variants_in_TFBS" : "  _  ".join(TFBS_table["Variants"]) }
    else : 
        return {"Transcription_Factor" : TF, "Variants_in_targeted_genes_regulatory_region_number" : len(Genes_variants_table), "Variants_in_TFBS_nb" : 0, "Variants_in_TFBS" : "0"}
  
  else : 
    return {"Transcription_Factor" : TF, "Variants_in_targeted_genes_regulatory_region_number" : len(Genes_variants_table), "Variants_in_TFBS_nb" : None, "Variants_in_TFBS" : None}




def select_variants(reference_genome:str, JASPAR_version:str, TF_gene_region_file, VCF_file, output_variants_file, follow_process_path, sep_table, regulatory_gene_distance:int, are_regulatory_region_given:bool, TFBS_files:str, is_regulatory_direction_given:bool) : 
  '''
  '''

  t0 = time.time()

  # asserts is a .md file 
  if follow_process_path[-3:] != ".md" :
      raise ValueError(f"'follow_process_path' must be a markdown (.md) file not {follow_process_path.split('.')[-1]}.")
  
  # Checks existence of input files : 
  if not os.path.exists("/".join(follow_process_path.split("/")[:-1])):
    os.makedirs("/".join(follow_process_path.split("/")[:-1]))

  follow_process_dir = "/".join(follow_process_path.split("/")[:-1])

  if not os.path.exists(TF_gene_region_file):
      raise ValueError(f"{TF_gene_region_file} does not exist. Change input.")

  if not os.path.exists(VCF_file):
      raise ValueError(f"{VCF_file} does not exist. Change input.")
  
  input_file_size = os.path.getsize(TF_gene_region_file) + os.path.getsize(VCF_file)

  if not TFBS_files : 
    if reference_genome != "hg38" :
      raise ValueError(f"If TFBS are not given, to request JASPAR Database correctly, it is better to convert genome coordinates to hg38 reference genome instead of {reference_genome} to have latest TFBS data.")
    
    for TFBS_file in TFBS_files.split(", ") :
      if not os.path.exists(TFBS_file) :
        raise ValueError(f"{TFBS_file} does not exist. Change input.")
      
      if (os.path.getsize(TFBS_file) / 1024) > 200000 : 
        raise ValueError(f"TFBS_file {TFBS_file} is too big, please split your file in ~ 100MB files.")
      input_file_size += TFBS_file
  
  if (not are_regulatory_region_given) | (not TFBS_files) :
    if not reference_genome :
      raise ValueError(f"reference_genome must be specified  (hg38 or hg19).")


  if not are_regulatory_region_given : 
    if (int(regulatory_gene_distance) < 0) | (int(regulatory_gene_distance) > 2000000) :
      raise ValueError(f"regulatory_gene_distance must be > 0 and < 2000000 bases and not {regulatory_gene_distance}, please modify input.")

  
  if reference_genome : 
    if (reference_genome != "hg38") & (reference_genome != "hg19") :
      raise ValueError(f"reference_genome must be hg38 or hg19 and not {reference_genome}, please modify input.")

  if not TFBS_files : 
    if not JASPAR_version : 
      JASPAR_version = "2020"

    
  
  # markdown file to follow process :
  text_info = (f"""

# <font color=orange>Module Select_variants.py :</font> 

lauched at {datetime.datetime.today()}

with arguments : 

| Arguments | |
| --- | --- |
| reference_genome | {reference_genome} |
| JASPAR_version | {JASPAR_version} |
| TF_gene_region_file | {TF_gene_region_file} |
| output_variants_file | {output_variants_file} |
| sep_table | {sep_table} |
| VCF_file | {VCF_file} |
| regulatory_gene_distance | {regulatory_gene_distance} |
| are_regulatory_region_given | {are_regulatory_region_given} |
| TFBS_files | {TFBS_files} |
| follow_process_path | {follow_process_path} |
| is_regulatory_direction_given | {is_regulatory_direction_given} |  


""")

  print(f"""

********************************************************************************
--> Module Select_variants.py :
lauched at {datetime.datetime.today()}
********************************************************************************

""")

  if not os.path.exists("/".join(follow_process_path.split("/")[:-1])):
      os.makedirs("/".join(follow_process_path.split("/")[:-1]))

  with open(follow_process_path, "a") as info_file:
      # Informations on the srcipt :
      info_file.write(text_info)

  # 1) Search gene's regulatory regions (start, end, chrom, strand): 

  if are_regulatory_region_given : 
    if is_regulatory_direction_given : 
      TF_gene_table = pd.read_csv(TF_gene_region_file, sep = sep_table, header = 0, engine = "python", usecols = ["Transcription_Factor", "Gene", "Reg", "chrom", "start", "end"]).drop_duplicates()
    else : 
      TF_gene_table = pd.read_csv(TF_gene_region_file, sep = sep_table, header = 0, engine = "python", usecols = ["Transcription_Factor", "Gene", "chrom", "start", "end"]).drop_duplicates()

    TF_gene_table = TF_gene_table.rename(columns = {"chrom" : "CHROM"})

    # if chromosome are not written just as a integer but with a string (to cope with XX et XY)
    i = 0
    while TF_gene_table["CHROM"][i].count("X") != 0 : 
      i += 1
    if not TF_gene_table["CHROM"][i].isdigit():
      TF_gene_table["CHROM"] = TF_gene_table["CHROM"].str.strip("".join(list(filter(lambda x: x.isalpha(), TF_gene_table["CHROM"][i]))))
      TF_gene_table["CHROM"] = TF_gene_table["CHROM"].astype(str)

    Gene_region_table = TF_gene_table[["Gene", "CHROM", "start", "end"]].drop_duplicates()
    # TF_gene_table = // mistakes here ?
    TF_gene_table.drop(["CHROM", "start", "end"], axis = 1).drop_duplicates()

  else : 
    Gene_region_table = pd.read_csv(TF_gene_region_file, sep = sep_table, header = 0, usecols = ["Gene"], engine = "python").drop_duplicates()
    if (reference_genome == "hg19") :
      print("Use reference genome hg19.")
      Gene_region_table[["start", "end", "chrom"]] = Gene_region_table.apply(lambda row : get_region_loc_gene_grch37(row["Gene"], int(regulatory_gene_distance), follow_process_path), axis = 1, result_type = "expand")  #  , "strand"]]
    else : 
      print("Use reference genome hg38.")
      Gene_region_table[["start", "end", "chrom"]] = Gene_region_table.apply(lambda row : get_region_loc_gene_grch38(row["Gene"], int(regulatory_gene_distance), follow_process_path), axis = 1, result_type = "expand")  #  , "strand"]]
    
    print(f"Regulatory region found at {datetime.datetime.today()}")
    # Rename to allow merge on CHROM with variants 
    Gene_region_table = Gene_region_table.rename(columns = {"chrom" : "CHROM"}) 
    
    # Drop unfounded genes (used start but could be end or chrom)
    Gene_region_table = Gene_region_table[Gene_region_table["start"] != None]

    # if chromosome are not written just as a integer but with a string (to cope with XX et XY)
    i = 0
    while Gene_region_table["CHROM"][i].count("X") != 0 : 
      i += 1
    if not Gene_region_table["CHROM"][i].isdigit():
      Gene_region_table["CHROM"] = Gene_region_table["CHROM"].str.strip("".join(list(filter(lambda x: x.isalpha(), Gene_region_table["CHROM"][i]))))
      Gene_region_table["CHROM"] = Gene_region_table["CHROM"].astype(str)

    
    # Gather gene's regulatory regions to all relations : 
    if is_regulatory_direction_given : 
      TF_gene_table = pd.read_csv(TF_gene_region_file, sep = sep_table, header = 0, engine = "python", usecols = ["Transcription_Factor", "Gene", "Reg"]).drop_duplicates()
    else : 
      TF_gene_table = pd.read_csv(TF_gene_region_file, sep = sep_table, header = 0, engine = "python", usecols = ["Transcription_Factor", "Gene"]).drop_duplicates()

  # calculates accessibles base number per TF for all initial TF in order to make a mutation proportion with the variants in gene regulatory region number

  all_TF = TF_gene_table.merge(Gene_region_table, how = "inner")

  all_TF["base_nb"] = all_TF["end"].astype(int) - all_TF["start"].astype(int)
  all_TF = all_TF[["Transcription_Factor", "base_nb"]].groupby(["Transcription_Factor"])["base_nb"].sum().reset_index()

  
  #print(TF_gene_table.head())

  # # Makes a table with a line for each TF (TF / Genes / Regions associated to genes (list of dict)) :
  # if is_strand_given : 
  #   TF_gene_table["Region_data"] = '{"start : "' + TF_gene_table["start"].astype(str) + ', "end" : ' + TF_gene_table["end"].astype(str) + ', "CHROM" : ' + TF_gene_table["CHROM"].astype(str) + ', "strand" : ' + TF_gene_table["strand"].astype(str) + '}'
  
  # else : 
  #   TF_gene_table["Region_data"] = '{"start : "' + TF_gene_table["start"].astype(str) + ', "end" : ' + TF_gene_table["end"].astype(str) + ', "CHROM" : ' + TF_gene_table["CHROM"].astype(str) + '}'

  # TF_gene_table = TF_gene_table[["Transcription_Factor", "Gene", "Reg", "Region_data"]].groupby(["Transcription_Factor", "Gene", "Reg"])["Region_data"].apply(lambda x : ", ".join(x)).rename("Regions").reset_index()
  
  # if is_regulatory_direction_given : 
  #   TF_gene_table["Genes_Regions_data"] = '{' + TF_gene_table['Gene'] + '_' + TF_gene_table["Reg"] + ' : ' + TF_gene_table["Regions"] + '}'  # { Gene_Reg : Regions }
  # else : 
  #   TF_gene_table["Genes_Regions_data"] = '{' + TF_gene_table['Gene'] + ' : ' + TF_gene_table["Regions"] + '}'  # { Gene : Regions }

  # TF_gene_table = TF_gene_table[["Transcription_Factor", "Genes_Regions_data"]]



  # 3) Search which genes have variants in their regulatory region (to reduce search space) and oppositely, which variants are in a gene's regulatory region  : 

  VCF_table = read_vcf(VCF_file)

  # if chromosome are not written just as a integer but with a string (to cope with XX et XY)
  i = 0
  while VCF_table["CHROM"][i].count("X") != 0 : 
    i += 1
  if not VCF_table["CHROM"][i].isdigit():
    VCF_table["CHROM"] = VCF_table["CHROM"].str.strip("".join(list(filter(lambda x: x.isalpha(), VCF_table["CHROM"][i]))))
    VCF_table["CHROM"] = VCF_table["CHROM"].astype(str)

  # Filter first genes having variants in its regulatory region 

  # Genes_variants_table = Gene_region_table.merge(VCF_table[["POS", "CHROM"]].drop_duplicates(), how = "inner")  # On CHROM normaly 
  Genes_variants_table = Gene_region_table.merge(VCF_table.drop_duplicates(), how = "inner")  # Keep variants annotations 
  Gene_region_table = []
  VCF_table = []

  Genes_variants_table = Genes_variants_table[(Genes_variants_table["start"].astype(int) < Genes_variants_table["POS"].astype(int)) & (Genes_variants_table["POS"].astype(int) < Genes_variants_table["end"].astype(int))]  # Selects on position in a regulatory region

  # Keep a line for a Gene-variant_position relation 
  Genes_variants_table = Genes_variants_table.drop(["start", "end"], axis = 1).drop_duplicates()

  # Keep TF-Gene relation for the one Gene have a variant in its regulatory relation 
  TF_gene_table = TF_gene_table.merge(Genes_variants_table[["Gene"]], how = "inner")  

  if is_regulatory_direction_given : 
    TF_gene_table = TF_gene_table[["Transcription_Factor", "Gene", "Reg"]].drop_duplicates()
  else : 
    TF_gene_table = TF_gene_table[["Transcription_Factor", "Gene"]].drop_duplicates()


  # 4) Searchs if variants are in TFBSs :
  # search only for variants in regulatory regions of genes targeted
    
  if TFBS_files :

    print("Charging all TF of interest's TFBS.")
    if TFBS_files.count(",") != 0 :  # if they are several files 
      TFBS_table = pd.read_csv(TFBS_files.split(", ")[0], sep = sep_table, header = 0, engine = "python").merge(all_TF["Transcription_Factor"], how = "inner")  # Select TFBS for TF in input data
      for TFBS_file in TFBS_files.split(", ")[1:] :  # Do it for all TFBS files 
        TFBS_table = TFBS_table.append(pd.read_csv(TFBS_file, sep = sep_table, header = 0, engine = "python").merge(all_TF["Transcription_Factor"], how = "inner"))

      print("End to charge all TFBS for TF of interest.")

    else :  # if they is only one 
      TFBS_table = pd.read_csv(TFBS_files, sep = sep_table, header = 0)  

    # in order to have a line for each TF even if TFBS are not given for its TF
    TFBS_table = TFBS_table.merge(all_TF["Transcription_Factor"], how = "outer")  
    client = None
    schema = None
    print("Starts to launch is_variants_in_TFBS.")
    Output_variants_table = TF_gene_table[["Transcription_Factor"]].drop_duplicates().apply(lambda row : is_variant_in_TFBS(client, schema, row["Transcription_Factor"], TF_gene_table[TF_gene_table["Transcription_Factor"] == row["Transcription_Factor"]].merge(Genes_variants_table, how = "inner").drop_duplicates(), JASPAR_version, reference_genome, sep_table, TFBS_table[TFBS_table["Transcription_Factor"] == row["Transcription_Factor"]].drop_duplicates(), follow_process_path), axis = 1, result_type = "expand")
    print("Ends to launch is_variants_in_TFBS.")

  else : 

    # Initialize a client & load the schema document
    client = coreapi.Client()
    schema = client.get("http://jaspar.genereg.net/api/v1/docs")

    Output_variants_table = TF_gene_table[["Transcription_Factor"]].drop_duplicates().apply(lambda row : is_variant_in_TFBS(client, schema, row["Transcription_Factor"], TF_gene_table[TF_gene_table["Transcription_Factor"] == row["Transcription_Factor"]].merge(Genes_variants_table, how = "inner").drop_duplicates(), JASPAR_version, reference_genome, sep_table, None, follow_process_path), axis = 1, result_type = "expand")

  # Get TF that are not associated to variants 
  all_TF = all_TF.merge(Output_variants_table, how = "outer")  

  # define mutation proportion 
  # pd.set_option('display.float_format', '{:.2E}'.format)  # to have percentages in scientific notation

  all_TF.to_csv(output_variants_file, index = None, sep = "\t")

  # all_TF_perc = all_TF[(all_TF["Variants_in_targeted_genes_regulatory_region_number"] != None) &(all_TF["Variants_in_targeted_genes_regulatory_region_number"] != None)]
  # all_TF_perc = all_TF[(all_TF["Variants_in_TFBS_nb"] != None) & (all_TF["Variants_in_TFBS_nb"] != None)]

  print("Calculates mutational charge percentages")

  all_TF_perc = all_TF.dropna()

  print(all_TF_perc.head())

  print(all_TF_perc["Variants_in_TFBS_nb"])

  all_TF_perc["mutational_charge_per_Mb"] = (all_TF_perc["Variants_in_targeted_genes_regulatory_region_number"].astype(float)/ all_TF_perc["base_nb"].astype(float)) * 100 
  # all_TF_perc["TFBS_mut_perc"] = (all_TF_perc["Variants_in_TFBS_nb"].astype(float) / all_TF_perc["base_nb"].astype(float)) * 100

  all_TF = all_TF.merge(all_TF_perc, how = "outer")
  all_TF_perc = []

  if not os.path.exists("/".join(output_variants_file.split("/")[:-1])):
      os.makedirs("/".join(output_variants_file.split("/")[:-1]))

  all_TF.to_csv(output_variants_file, index = None, sep = "\t")  # sep = ast.literal_eval(sep_table))

  print("Stores statistical information")
  # statistics on mutational charge

  # pipeline.stat(all_TF[["TFBS_mut_perc"]], "TFBS_mut_perc", "hist, box", "\t", f"{follow_process_dir}/graphics")
  pipeline.stat(all_TF[["mutational_charge_per_Mb"]], "mutational_charge_per_Mb", "hist, box", "\t", f"{follow_process_dir}/graphics")
  pipeline.stat(all_TF[["Variants_in_targeted_genes_regulatory_region_number"]], "Variants_in_targeted_genes_regulatory_region_number", "hist, box", "\t", f"{follow_process_dir}/graphics")
  pipeline.stat(all_TF[["Variants_in_TFBS_nb"]], "Variants_in_TFBS_nb", "hist, box", "\t", f"{follow_process_dir}/graphics")
  pipeline.stat(all_TF[["base_nb"]], "base_nb", "hist, box", "\t", f"{follow_process_dir}/graphics")

#   text_info = f"""


# ## TFBS_mut_perc : 
# ***

# {pipeline.display_table_stat("graphics/TFBS_mut_perc_hist.png", "TFBS_mut_perc_hist", "graphics/TFBS_mut_perc_box.png" , "TFBS_mut_perc_box", pipeline.makes_percentiles_intervals(all_TF[["Transcription_Factor", "TFBS_mut_perc"]], "TFBS_mut_perc").to_markdown())}

# ***
# """

  text_info += f"""


## mutational_charge_per_Mb : 
***

{pipeline.display_table_stat("graphics/mutational_charge_per_Mb_hist.png", "mutational_charge_per_Mb_hist", "graphics/mutational_charge_per_Mb_box.png" , "mutational_charge_per_Mb_box", pipeline.makes_percentiles_intervals(all_TF[["Transcription_Factor", "mutational_charge_per_Mb"]], "mutational_charge_per_Mb").to_markdown())}

***
"""

  text_info += f"""


## Variants_in_targeted_genes_regulatory_region_number : 
***

{pipeline.display_table_stat("graphics/Variants_in_targeted_genes_regulatory_region_number_hist.png", "Variants_in_targeted_genes_regulatory_region_number_hist", "graphics/Variants_in_targeted_genes_regulatory_region_number_box.png" , "Variants_in_targeted_genes_regulatory_region_number_box", pipeline.makes_percentiles_intervals(all_TF[["Transcription_Factor", "Variants_in_targeted_genes_regulatory_region_number"]], "Variants_in_targeted_genes_regulatory_region_number").to_markdown())}

***
"""

  text_info += f"""


## Variants_in_TFBS_nb : 
***

{pipeline.display_table_stat("graphics/Variants_in_TFBS_nb_hist.png", "Variants_in_TFBS_nb_hist", "graphics/Variants_in_TFBS_nb_box.png" , "Variants_in_TFBS_nb_box", pipeline.makes_percentiles_intervals(all_TF[["Transcription_Factor", "Variants_in_TFBS_nb"]], "Variants_in_TFBS_nb").to_markdown())}

***
"""

  text_info += f"""


## base_nb : 
***

{pipeline.display_table_stat("graphics/base_nb_hist.png", "base_nb_hist", "graphics/base_nb_box.png" , "base_nb_box", pipeline.makes_percentiles_intervals(all_TF[["Transcription_Factor", "base_nb"]], "base_nb").to_markdown())}

***
"""


  # Execution time 
  print(f"Ends at {round((time.time() - t0), 3)} seconds <=> {round(((time.time() - t0)/60), 3)} minutes")
  text_info += f"\nExecution time : {round((time.time() - t0), 3)} seconds <=> {round(((time.time() - t0)/60), 3)} minutes"

  with open(follow_process_path, "a") as follow_process_file :
      follow_process_file.write(text_info)
  # execution_time (sec), execution_time (min), memory_usage (MiB), relations_table_size (MiB)
  print(int(getrusage(RUSAGE_SELF).ru_maxrss / 1024))
  return [round((time.time() - t0), 3), round(((time.time() - t0)/60), 3), int(getrusage(RUSAGE_SELF).ru_maxrss / 1024), int(input_file_size / 1024)]

if __name__ == "__main__" : 

    parser = argparse.ArgumentParser()

    parser.add_argument("--reference_genome", help = "Reference genome used and to use to search genome coordinates.")
    parser.add_argument("-s", "--sep_table", help = "String that separates columns in transcription table of interest.")
    parser.add_argument("--JASPAR_version", help = "If transcription factor binding sites must be search, precise which version of JASPAR database use (2020, 2018...)")
    parser.add_argument("--TF_gene_region_file", help = "Relations_table path, can contain or not regions coordinates.")
    parser.add_argument("--VCF_file", help = "Path to the Variant Calling Format file.")
    parser.add_argument("--output_variants_file", help = "Path to the file to output (with variants per transcription factor found).")
    parser.add_argument("--regulatory_gene_distance", help = "Distance between a gene and its regulatory regions to consider (ex : 500000 bases).")
    parser.add_argument("--are_regulatory_region_given", help = "Bool, if regions' genomic coordinates are already given in the relation table (start, end, chrom).")
    parser.add_argument("--TFBS_files", help = "Path to the transcription binding sites file if already search, otherwise, search on JASPAR.")
    parser.add_argument("--follow_process_path", help = "Path to the file to save process data.")
    parser.add_argument("--is_regulatory_direction_given", help = "Bool, if regulatory direction is given as a column in relations table.")
    
    args = parser.parse_args()

    select_variants(args.reference_genome, args.JASPAR_version, args.TF_gene_region_file, args.VCF_file, args.output_variants_file, args.follow_process_path, args.sep_table, int(args.regulatory_gene_distance), args.are_regulatory_region_given, args.TFBS_files, args.is_regulatory_direction_given)

    # 1) sans données, en utilisant les bases de données :
    # /usr/bin/python3 /home/eveadmin/Bureau/Class_Factory/select_variants.py --reference_genome hg38 --JASPAR_version 2020 --TF_gene_region_file /home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_no_no/Relations_tables/selected_relations.csv --VCF_file /home/eveadmin/Bureau/Variants/tests/remapped_hg38_S1_Candidates.vcf --output_variants_file /home/eveadmin/Bureau/Variants/tests/2_remapped_hg38_variants_output.csv --follow_process_path /home/eveadmin/Bureau/Variants/tests/follow_process.md --sep_table "\t" --regulatory_gene_distance 500000 --is_regulatory_direction_given True
    
    # 2) avec TFBS sans regions
    # /usr/bin/python3 /home/eveadmin/Bureau/Class_Factory/select_variants.py --reference_genome hg19 --TF_gene_region_file /home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_no_no/Relations_tables/selected_relations.csv --VCF_file /home/eveadmin/Bureau/Variants/tests/S1_Candidates.vcf --output_variants_file /home/eveadmin/Bureau/Variants/tests/hg19_TFBS_given_variants_output.csv --follow_process_path /home/eveadmin/Bureau/Variants/tests/follow_process.md --sep_table "\t" --regulatory_gene_distance 500000 --is_regulatory_direction_given True --TFBS_files "/home/eveadmin/Bureau/Variants/tests/TFBS_files/tf_loc_sub1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/tf_loc_sub2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/tf_loc_sub3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/tf_loc_sub4.bed"    
    
    # 3) sans TFBS avec regions
    # /usr/bin/python3 /home/eveadmin/Bureau/Class_Factory/select_variants.py --reference_genome hg38 --JASPAR_version 2020 --TF_gene_region_file /home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_no_no/Relations_tables/remapped_region_selected_relations.csv --VCF_file /home/eveadmin/Bureau/Variants/tests/remapped_hg38_S1_Candidates.vcf --output_variants_file /home/eveadmin/Bureau/Variants/tests/hg38_regions_given_variants_output.csv --follow_process_path /home/eveadmin/Bureau/Variants/tests/follow_process.md --sep_table "\t" --regulatory_gene_distance 500000 --is_regulatory_direction_given True --are_regulatory_region_given True
    # /usr/bin/python3 /home/eveadmin/Bureau/Class_Factory/select_variants.py --reference_genome hg38 --JASPAR_version 2020 --TF_gene_region_file /home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_no_no/Relations_tables/remapped_region_selected_relations.csv --VCF_file /home/eveadmin/Bureau/Variants/tests/remapped_hg38_S1_Candidates.vcf --output_variants_file /home/eveadmin/Bureau/Variants/tests/hg38_regions_given_variants_output.csv --follow_process_path /home/eveadmin/Bureau/Variants/tests/follow_process.md --sep_table "\t" --regulatory_gene_distance 500000 --is_regulatory_direction_given True --are_regulatory_region_given True

    # 4) avec TFBS et regions 
    # /usr/bin/python3 /home/eveadmin/Bureau/Class_Factory/select_variants.py --TF_gene_region_file /home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_no_no/Relations_tables/coordinates_region_selected_relations.csv --VCF_file /home/eveadmin/Bureau/Variants/tests/S1_Candidates.vcf --output_variants_file /home/eveadmin/Bureau/Variants/tests/hg19_TFBS_region_given_variants_output.csv --follow_process_path /home/eveadmin/Bureau/Variants/tests/follow_process.md --sep_table "\t" --regulatory_gene_distance 500000 --is_regulatory_direction_given True --TFBS_files "/home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_6.bed" --are_regulatory_region_given True
    
    # , /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_6.bed
    # , /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_6.bed
    # , /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_6.bed
    # , /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_6.bed

    # , /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_6.bed

    # S1
    # /usr/bin/python3 /home/eveadmin/Bureau/Class_Factory/select_variants.py --TF_gene_region_file /home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_no_no/Relations_tables/coordinates_region_selected_relations.csv --VCF_file /home/eveadmin/Bureau/Variants/S1/Candidates.vcf --output_variants_file /home/eveadmin/Bureau/Variants/S1/hg19_TFBS_region_given_variants_output.csv --follow_process_path /home/eveadmin/Bureau/Variants/S1/follow_process.md --sep_table "\t" --regulatory_gene_distance 500000 --is_regulatory_direction_given True --TFBS_files "/home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_6.bed" --are_regulatory_region_given True

    # S2
    # /usr/bin/python3 /home/eveadmin/Bureau/Class_Factory/select_variants.py --TF_gene_region_file /home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_no_no/Relations_tables/coordinates_region_selected_relations.csv --VCF_file /home/eveadmin/Bureau/Variants/S2/Candidates.vcf --output_variants_file /home/eveadmin/Bureau/Variants/S2/hg19_TFBS_region_given_variants_output.csv --follow_process_path /home/eveadmin/Bureau/Variants/S2/follow_process.md --sep_table "\t" --regulatory_gene_distance 500000 --is_regulatory_direction_given True --TFBS_files "/home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_6.bed" --are_regulatory_region_given True

    # S3
    # /usr/bin/python3 /home/eveadmin/Bureau/Class_Factory/select_variants.py --TF_gene_region_file /home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_no_no/Relations_tables/coordinates_region_selected_relations.csv --VCF_file /home/eveadmin/Bureau/Variants/S3/Candidates.vcf --output_variants_file /home/eveadmin/Bureau/Variants/S3/hg19_TFBS_region_given_variants_output.csv --follow_process_path /home/eveadmin/Bureau/Variants/S3/follow_process.md --sep_table "\t" --regulatory_gene_distance 500000 --is_regulatory_direction_given True --TFBS_files "/home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_6.bed" --are_regulatory_region_given True

    # S4
    # /usr/bin/python3 /home/eveadmin/Bureau/Class_Factory/select_variants.py --TF_gene_region_file /home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_no_no/Relations_tables/coordinates_region_selected_relations.csv --VCF_file /home/eveadmin/Bureau/Variants/S4/Candidates.vcf --output_variants_file /home/eveadmin/Bureau/Variants/S4/hg19_TFBS_region_given_variants_output.csv --follow_process_path /home/eveadmin/Bureau/Variants/S4/follow_process.md --sep_table "\t" --regulatory_gene_distance 500000 --is_regulatory_direction_given True --TFBS_files "/home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_6.bed" --are_regulatory_region_given True

    # S5
    # /usr/bin/python3 /home/eveadmin/Bureau/Class_Factory/select_variants.py --TF_gene_region_file /home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_no_no/Relations_tables/coordinates_region_selected_relations.csv --VCF_file /home/eveadmin/Bureau/Variants/S5/Candidates.vcf --output_variants_file /home/eveadmin/Bureau/Variants/S5/hg19_TFBS_region_given_variants_output.csv --follow_process_path /home/eveadmin/Bureau/Variants/S5/follow_process.md --sep_table "\t" --regulatory_gene_distance 500000 --is_regulatory_direction_given True --TFBS_files "/home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_6.bed" --are_regulatory_region_given True

    # S6
    # /usr/bin/python3 /home/eveadmin/Bureau/Class_Factory/select_variants.py --TF_gene_region_file /home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_no_no/Relations_tables/coordinates_region_selected_relations.csv --VCF_file /home/eveadmin/Bureau/Variants/S6/Candidates.vcf --output_variants_file /home/eveadmin/Bureau/Variants/S6/hg19_TFBS_region_given_variants_output.csv --follow_process_path /home/eveadmin/Bureau/Variants/S6/follow_process.md --sep_table "\t" --regulatory_gene_distance 500000 --is_regulatory_direction_given True --TFBS_files "/home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub1_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub2_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub3_6.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_1.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_2.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_3.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_4.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_5.bed, /home/eveadmin/Bureau/Variants/tests/TFBS_files/split_tf_loc/sub4_6.bed" --are_regulatory_region_given True
