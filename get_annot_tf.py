#! /usr/bin/env python
# -*- coding: utf-8 -*-


'''
@author : Ève Barré

Get_annot_tf provides several TF annotations in order to highlight known TF in the biological context in question. 
Possible annotations are : 
- a) if the TF is associated with Gene Ontology (GO) terms of interest (graph_go helps the choice);
- b) the count of TF's quotations in pubmed database papers in tree possible conditions : 
    - The first count depict all TF citation whatever the paper, 
    - the second count one defines that articles must be annotated by a given general combinaison of keywords (Medical Subject Headings MeSH terms), 
    - and a more stringent stringent one using logical operators AND and OR too (see get_MeSH to choice);
- c) keywords associated to TF in uniprot database (https://sparql.uniprot.org/sparql/), in order to visualize if TF is not directly annotated by GO terms of interest, 
in which biological process, disease, molecular function for example (ten possible user_adjustable keywords categories) it is known.
The serach of annotation can be done in the first part, before any TF selection or after (get_annot_tf_2). 

This module require stable internet connection in order to query databases.

Remarque : it is possible that gene encoded for several proteins.  

--> Arguments : 

    "-tf", "--list_TF" : Path to the transcription factor's list selected by Regulus, it can also be a relation table, must have a "Transcription Factor" column label.
    "-s", "--sep_table" : String that separates columns in transcription table of interest, example : '\t', ',', ....
    "--email" : email address to informs who launch query to nbci (mandatory).
    "-go", "--list_go_interest" : String of go terms of interest separated by ', '. Example : 'cellular developmental process:0048869, immune system process:0002376, ... Choice helped by graph_go module.
    "-kw", "--list_unwanted_keywords" : Specific keywords that are not wanted separated by ', ', example : 'DNA-binding, Repressor, Activator, Transcription regulation'.
    "-ckw", "--list_categories_keywords" : Wanted categories' keywords (as : Molecular function, Biological Process...) 
    "-aan", "--already_annotation_table" : Path to the all transcription factor annotated table.
    "--dict_annot_columns_id" : Dictionnary of annotations columns number given by an other query (in the follow process file) to reuse annotations of a table.
    "-strict_quote", "--strict_MeSH_terms" : Maximum list of MeSH terms (labels) that papers must be annotated with to be counted, (choice help with get_MeSH module) example : (Plasma Cells OR B-Lymphocytes OR Lymphocyte Activation OR Germinal Center) AND (Transcription Factors OR Gene Expression Regulation) AND Cell Differentiation.
    "-large_quote", "--large_MeSH_terms" : Maximum list of MeSH terms (labels) that papers must be annotated with to be counted, (choice help with get_MeSH module) example : (B-Lymphocytes OR Plasma Cells).
    "-quote", "--get_G_quotations_nb" :   Bool, if general papers' quotation for this TF number must be return as a column.
    "--follow_process_path" : Path to the file to save process data.
    "--path_TF_table_annotated" : Path to transcription factors annotated file.

--> Proceeding : 

- writes all along the script process data informations in a file (follow_process_path)
    - script name
    - launch date and time 
    - arguments 
    - Statistics on numerics TF table datas
    - Statistics on numerics TF table datasStatistics on strict_quotations_nb
    - Statistics on numerics TF table datasStatistics on large_quotations_nb
    - Statistics on numerics TF table datasStatistics on general_quotations_nb
    - Number of TF's patterns per number of TF per TF's patterns
    - Table of number of TF with anntation and their labels
    - Keywords count on each keywords' category
    - execution time and memory use
    - print columns labels directory if wanred to reuse the TF annotated table

- (tf) imports transcription factors list, with separation defined (sep_table)
- (already_annotation_table) uses annotation table if already calculated from columns index given (dict_annot_columns_id)(col_cat, col_go, col_quote, col_G_quote, col_strict_quote, col_large_quote)
- if not : search for each transcription factors : 
    - all paper quotations count
    - paper quotations with strict MeSH terms combination
    - paper quotations with large MeSh term combination
    - if is annoted by go terms of interest (go) and how many TF in GO database are annotated too 
    - keywords associated of a given category (ckw) and whch are not unwanted keyword (kw)
- calculated percentage of paper quotations that are assocaited with MeSH terms of interest for each transcription factors
- stores columns labels in a dictionnary if reuse this annotated TF table
- stores transcription factors' annotation table 

--> In pipeline : 
Get_annot_tf can be launch before or after TF selection and filtration. 
It is recommanded to use get_tf_annotation in the first part of pipeline in case of multiple query 
because the annotated TF table can be directly reused instead of research again annotation for same TF (can be quite long).

Go terms of interest choice can be support by graph_go module as queries on 
papers' quotations with get_MeSH who provides from a paper's set keywords count. 

--> Usage tips : 

Unwanted keywords can be define after a previous query containing numerous of uninteresting keywords.
List of keywords category can be chosen with the biological context, there are ten : 
disease, molecular function, biological process, cellular component, coding sequence diversity, developmental stage, domain, ligand, post-traductional modification, technical term.

--> Execution time and memory evolution : 

depends on : 

- get gereral quotation number/large_MeSH_terms/strict_MeSH_terms : Yes / No
- nb go terms of interest to request 
- if annotation already done 
- nb TF

'''

from SPARQLWrapper import SPARQLWrapper, JSON
from Bio import Entrez
import argparse
import pandas as pd
import time
import datetime
import os 
import matplotlib.pyplot as plt 
from resource import getrusage, RUSAGE_SELF  # to assess the peak memory usage
import ast
import pipeline # to have stat and display_stat


# function needed : 

def get_TF_go_annot(email:str, Gene:str, list_go_interest:[], list_categories_keywords:[], list_unwanted_keywords:[], dict_annot_columns_id:{}, large_MeSH_terms:str, strict_MeSH_terms:str, get_G_quotations_nb:bool) -> [] :
    '''
    A function to inform if transcription factor of interest's is annotated by go terms of interest.

    Args : 
        Gene([]) : current gene name that encodes the transcription factor of interest.

    Warning, list_go_interest is previous by transcription factors count having this annotation (separated by '_')
    Returns:
        A list of keywords' label that annotate the transcription factor of interest.

    ''' 

    ## Counts of transcription factor quotations

    Entrez.email = email

    col_annot = {}

    if get_G_quotations_nb :
        ### All TF quotations :
        handle = Entrez.egquery(term = (f"{Gene}"))
        record = Entrez.read(handle)
        for row in record["eGQueryResult"] :
            if row["DbName"] == "pubmed" :
                col_annot["G_quotations_nb"] = row["Count"]

    ### MeSH minimum quotations :
    # query_min = ""
    # for min_MeSH in large_MeSH_terms.split(", ") :
    #     query_min += f"{min_MeSH} [Mesh] OR"

    if large_MeSH_terms : 
        query = (large_MeSH_terms + f" AND {Gene}")
        handle = Entrez.egquery(term = query)  # f"Transcription Factors AND B-Lymphocytes AND {Gene}")
        record = Entrez.read(handle)
        for row in record["eGQueryResult"] :
            if row["DbName"] == "pubmed" :
                col_annot["large_quotations_nb"] = row["Count"]

    ### MeSH maximum quotations :
    # query_max = f"{Gene}"
    # for max_MeSH in strict_MeSH_terms.split(", ")[1:] :
    #     query_max += f" AND {max_MeSH} [Mesh]"

    # handle = Entrez.egquery(term = (strict_MeSH_terms + f"AND {Gene}"))  # (query_max))
    
    if strict_MeSH_terms : 
        query = (strict_MeSH_terms + f" AND {Gene}")
        handle = Entrez.egquery(term = query)  
        # f"(Plasma Cells OR B-Lymphocytes OR Lymphocyte Activation) AND Cell Differentiation AND Transcription Factors AND {Gene}")
        record = Entrez.read(handle)
        for row in record["eGQueryResult"] :
            if row["DbName"] == "pubmed" :
                col_annot["strict_quotations_nb"] = row["Count"]

    # Could be with uniprot but more precise with pubmed

    sparql = SPARQLWrapper("https://sparql.uniprot.org/sparql/")

    # sparql.setQuery("""
    #     PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
    #     PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
    #     PREFIX owl: <http://www.w3.org/2002/07/owl#> 
    #     PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> 
    #     PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
    #     PREFIX up: <http://purl.uniprot.org/core/>
    #     PREFIX taxon: <http://purl.uniprot.org/taxonomy/>
    #     PREFIX go: <http://purl.obolibrary.org/obo/GO_>

    #     SELECT DISTINCT (COUNT(DISTINCT ?quotation) AS ?nb_cit)

    #     WHERE {    
    #         VALUES ?gene_label {'""" + Gene + """'}  

    #         ?gene a up:Gene .
    #         ?gene skos:prefLabel ?gene_label .

    #         ?TF up:encodedBy ?gene .
    #         ?TF a up:Protein . 
    #         ?TF up:organism taxon:9606 .
            
    #         ?TF up:quotation ?quotation .
    #         ?quotation rdf:type up:Journal_quotation .
        
    #     }

    # """)


    # sparql.setReturnFormat(JSON)
    # results = sparql.query().convert()

    # # Considers this number as int and not str in order to allow comparison on it ?
    # nb_cit = int([result["nb_cit"]["value"] for result in results["results"]["bindings"]][0]) 

    # if nb_cit == []:
    #     col_annot = {"nb_quotation":0}

    # else : col_annot = {"nb_quotation":nb_cit}


    if list_go_interest : 
        # Tells if TF is annotated by go terms of interest :

        # Writes in sparql go terms of interest to filter : 
        filter_sparql_go = f"(regex(?go, go:{list_go_interest[0][-7:]}))"  # to have only GO identifier
        for c_go in list_go_interest[1:] : 
            filter_sparql_go += f" || (regex(?go, go:{c_go[-7:]}))"


        sparql.setQuery("""
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
            PREFIX owl: <http://www.w3.org/2002/07/owl#> 
            PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> 
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
            PREFIX up: <http://purl.uniprot.org/core/>
            PREFIX taxon: <http://purl.uniprot.org/taxonomy/>
            PREFIX go: <http://purl.obolibrary.org/obo/GO_>

            SELECT DISTINCT ?go_label

            WHERE {    
                VALUES ?gene_label {'""" + Gene + """'}  

                ?gene a up:Gene .
                ?gene skos:prefLabel ?gene_label .

                ?TF up:encodedBy ?gene .
                ?TF a up:Protein . 
                ?TF up:organism taxon:9606 .

                ?TF up:classifiedWith/(rdfs:subClassOf)* ?go .  
                ?go rdf:type owl:Class .
                ?go rdfs:label ?go_label . 

                FILTER (""" + filter_sparql_go + """) .
                
            }

        """)


        sparql.setReturnFormat(JSON)
        results = sparql.query().convert()

        go_found = [result["go_label"]["value"] for result in results["results"]["bindings"]]

        for c_go in list_go_interest : 
            if c_go.split("_")[1][:-8] in go_found :  # to have only go term's label 
                col_annot[c_go[:-8]] = "X"  # TF count having this go term + its label (separated by '_')
            else :
                col_annot[c_go[:-8]] = "_"


    if list_categories_keywords : 
        ## Searchs for keywords that annotates TF :

        # Writes in sparql keywords' categories to select and unwanted keywords :
        filter_sparql_cat = f"(regex(?category_label, '{list_categories_keywords[0]}'))"
        for c_category in list_categories_keywords[1:] : 
            filter_sparql_cat += f" || (regex(?category_label, '{c_category}'))"


        if list_unwanted_keywords : 
            filter_sparql_key = f"(!regex(?keyword_label, '{list_unwanted_keywords[0]}'))" 
            for c_keyword in list_unwanted_keywords[1:] : 
                filter_sparql_key += f" && (!regex(?keyword_label, '{c_keyword}'))"
            
            filter_sparql_key = "FILTER (" + filter_sparql_key + ") ."

        else : 
            filter_sparql_key = ""

        sparql.setQuery("""
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
            PREFIX owl: <http://www.w3.org/2002/07/owl#> 
            PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> 
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
            PREFIX up: <http://purl.uniprot.org/core/>
            PREFIX taxon: <http://purl.uniprot.org/taxonomy/>

            SELECT DISTINCT ?category_label (GROUP_CONCAT(DISTINCT ?keyword_label; separator = ", ") AS ?keywords)
            WHERE {

                VALUES ?gene_label {'""" + Gene + """'}  

                ?gene a up:Gene .
                ?gene skos:prefLabel ?gene_label .

                ?TF up:encodedBy ?gene .
                ?TF a up:Protein . 
                ?TF up:organism taxon:9606 . # Homo Sapiens
                ?TF up:classifiedWith ?keyword .

                ?keyword rdf:type <http://purl.uniprot.org/core/Concept> .
                ?keyword up:category/skos:prefLabel ?category_label .
                ?keyword skos:prefLabel ?keyword_label . 

                FILTER (""" + filter_sparql_cat + """) .
                """ + filter_sparql_key + """
            }

            GROUP BY ?category_label
        """)

        sparql.setReturnFormat(JSON)
        results = sparql.query().convert()
        
        categories = [result["category_label"]["value"] for result in results["results"]["bindings"]]
        keywords = [result["keywords"]["value"] for result in results["results"]["bindings"]]

        for c_category in list_categories_keywords : 
            if c_category in categories :
                col_annot[c_category] = keywords[categories.index(c_category)]
            else : 
                col_annot[c_category] = ""

    return col_annot

def get_annot_tf(email:str, path_TF_table_annotated:str, follow_process_path:str, list_TF:str, sep_table:str, list_go_interest:str, list_unwanted_keywords:str, list_categories_keywords:[str], already_annotation_table, dict_annot_columns_id:str, strict_MeSH_terms:str, large_MeSH_terms:str, get_G_quotations_nb:bool) :
    '''

    '''

    t0 = time.time()

    # Checks existence of input files : 
    if not os.path.exists(list_TF):
        raise ValueError(f"{list_TF} does not exist. Change input.")

    # Checks if arguments are missing : 
    if not list_TF : 
        raise ValueError("Input file missing. (--list_TF)")

    # asserts is a .md file 
    if follow_process_path[-3:] != ".md" :
        raise ValueError(f"'follow_process_path' must be a markdown (.md) file not {follow_process_path.split('.')[-1]}.")

    if not os.path.exists("/".join(follow_process_path.split("/")[:-1])):
        os.makedirs("/".join(follow_process_path.split("/")[:-1]))

    follow_process_dir = "/".join(follow_process_path.split("/")[:-1])

    if list_go_interest :
        if list_go_interest.count(":") == 0 : 
            raise ValueError(f"list_go_interst must be written as ', ' between each go terms an ':' between a label and its identifier like : 'transcription regulator activity:0140110, immune system process:0002376'")
        if not all([((type(go.split(':')[0]) == str) & (go.split(':')[1].isdigit()) & (len(go.split(':')[1]) == 7)) for go in list_go_interest.split(", ")]) :
            raise ValueError(f"list_go_interst must be written as ', ' between each go terms an ':' between a label and its identifier (that must have a length of 7 digit).\nExample : 'transcription regulator activity:0140110, immune system process:0002376'")

    if already_annotation_table :
        if not os.path.exists(already_annotation_table):
            raise ValueError(f"{already_annotation_table} does not exist. Change input.")
        if not dict_annot_columns_id :
            raise ValueError(f"If reuse a annotated TF table, must specify annotation columns number. ")

    # markdown_file to follow process :
    text_info = (f"""

# <font color=orange>Module get_annot_tf.py :</font> 

lauched at {datetime.datetime.today()}

with arguments : 

| Arguments | |
| --- | --- |
| path_TF_table_annotated | {path_TF_table_annotated} |
| follow_process_path | {follow_process_path} |
| list_TF | {list_TF} |
| sep_table | {sep_table} |
| list_go_interest | {list_go_interest} |
| list_unwanted_keywords | {list_unwanted_keywords} |
| list_categories_keywords | {list_categories_keywords} |
| already_annotation_table | {already_annotation_table} |
| dict_annot_columns_id | {dict_annot_columns_id} |
| strict_MeSH_terms | {strict_MeSH_terms} |
| large_MeSH_terms | {large_MeSH_terms} |
| get_G_quotations_nb | {get_G_quotations_nb} |


""")
    print(f"""

********************************************************************************
--> Module get_annot_tf.py :
lauched at {datetime.datetime.today()}
********************************************************************************

""")

    with open(follow_process_path, "a") as info_file:
        # Informations on the srcipt :
        info_file.write(text_info)
    
    input_size = os.path.getsize(list_TF)

    list_all_TF = pd.read_csv(list_TF, sep = sep_table, header = 0, engine='python') 

    list_TF = list_all_TF[["Transcription_Factor"]].drop_duplicates()
    print(f"Transcription factors' list to research annotation head is : \n{list_TF.head()}")

    print(f"\nDefining annotations to search since {datetime.datetime.today()}\n")

    # Checks if all TF annotation is already researched : 
    if already_annotation_table : 

        annot_table = pd.read_csv(already_annotation_table, sep = sep_table, header = 0, engine = 'python').drop_duplicates()

        if type(dict_annot_columns_id) == str : 
            dict_annot_columns_id = ast.literal_eval(dict_annot_columns_id)

        # Seletcs which columns imports in annotated table
        col_annot_to_use = []

        for col_annot in dict_annot_columns_id.values() :
            if type(col_annot) == list : 
                col_annot_to_use.extend(col_annot)
            else : 
                col_annot_to_use.append(col_annot)

        col_annot_to_use = [annot_table.columns[int(col_num)] for col_num in col_annot_to_use]
        col_annot_to_use.append("Transcription_Factor")

        list_TF = list_TF.merge(annot_table[col_annot_to_use].drop_duplicates(), how = "left")


        # Defines what are annotations of interest :
        if dict_annot_columns_id["columns_go_terms"] :
            list_go_interest = []
            for i_col in dict_annot_columns_id["columns_go_terms"] :
                list_go_interest.append(list(annot_table.columns)[int(i_col)])
        else : 
            list_go_interest = None

        if dict_annot_columns_id["columns_keywords_categories"] :
            list_categories_keywords = []
            for i_col in dict_annot_columns_id["columns_keywords_categories"] :
                list_categories_keywords.append(list(annot_table.columns)[int(i_col)])
        else : 
            list_categories_keywords = None

        get_G_quotations_nb = dict_annot_columns_id["column_G_quotations"]
        strict_MeSH_terms = dict_annot_columns_id["column_strict_MeSH_quotations"]
        large_MeSH_terms = dict_annot_columns_id["column_large_MeSH_quotations"]


    else :  # Searchs for annotations of interest if it is not already done 

        if list_go_interest : 
            list_go_interest = list_go_interest.split(", ")  # without the count of TF

        if list_unwanted_keywords : 
            list_unwanted_keywords = list_unwanted_keywords.split(", ")

        if list_categories_keywords : 
            list_categories_keywords = list_categories_keywords.split(", ")


    # Stores process and statistical informations in a txt file. 
    if not os.path.exists("/".join(follow_process_path.split("/")[:-1])):
        os.makedirs("/".join(follow_process_path.split("/")[:-1]))

    if not already_annotation_table : 
        # To have the transcription factor total number annotated by a go term of interest : 

        if list_go_interest : 
            new_go_col_name = []
            for c_go in list_go_interest :
                sparql = SPARQLWrapper("https://sparql.uniprot.org/sparql/")

                sparql.setQuery("""
                    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
                    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
                    PREFIX owl: <http://www.w3.org/2002/07/owl#> 
                    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> 
                    PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
                    PREFIX up: <http://purl.uniprot.org/core/>
                    PREFIX taxon: <http://purl.uniprot.org/taxonomy/>
                    PREFIX go: <http://purl.obolibrary.org/obo/GO_>

                    SELECT DISTINCT (COUNT(DISTINCT ?gene_label) AS ?TF_nb)

                    WHERE {    
            
                        VALUES ?go { go:""" + c_go[-7:] + """ } .
                        VALUES ?go_TF { go:0140110 } . # Transcription regulator activity 

                        ?gene a up:Gene .
                        ?gene skos:prefLabel ?gene_label .

                        ?TF up:encodedBy ?gene .
                        ?TF a up:Protein . 
                        ?TF up:organism taxon:9606 .

                        ?TF up:classifiedWith/(rdfs:subClassOf) ?go .  
                        ?go rdf:type owl:Class .
                        
                        ?TF up:classifiedWith/(rdfs:subClassOf) ?go_TF .  
                        ?go_TF rdf:type owl:Class .
                    }

                """)

                sparql.setReturnFormat(JSON)
                try  : 
                    results = sparql.query().convert()
                except :  # to handle with error
                    time.sleep(5)
                    results = sparql.query().convert()

                TF_nb = [result["TF_nb"]["value"] for result in results["results"]["bindings"]][0]
                new_go_col_name.append((str(TF_nb) + "_" + c_go))

                # adds TF_nb at the beginning of the go terms' label 

                list_go_interest = new_go_col_name 

        print(f"\nSearching annotations of {list_TF.shape[0]} transcription factors since {datetime.datetime.today()}.")

        df_go = list_TF.apply(lambda row: get_TF_go_annot(email, row["Transcription_Factor"], list_go_interest, list_categories_keywords, list_unwanted_keywords, {}, large_MeSH_terms, strict_MeSH_terms, get_G_quotations_nb), axis = 1, result_type = "expand")

        # Calculates interesting quotation percentage : adds columns next to columns of quotations number
        if strict_MeSH_terms : 
            df_go.insert((list(df_go.columns).index("strict_quotations_nb") + 1), "strict_quote_per", round(((df_go["strict_quotations_nb"].astype(int) / df_go["G_quotations_nb"].astype(int)) * 100), 1))
        if large_MeSH_terms : 
            df_go.insert((list(df_go.columns).index("large_quotations_nb") + 1), "large_quote_per", round(((df_go["large_quotations_nb"].astype(int) / df_go["G_quotations_nb"].astype(int)) * 100), 1)) 

        list_TF = list_TF.merge(df_go, right_index = True, left_index = True)

        print(f"Calculating statistics on data since {datetime.datetime.today()}.")

        # # prints information on annotations selected : 
        print("\nKeywords' category selected are : ")

        if list_categories_keywords :
            for c_category in list_categories_keywords : 
                print(f"- {c_category}")

        if list_go_interest :
            print("\nGO terms of interest are (with its labels) : ")
            for c_go in list_go_interest : 
                print(f"- {c_go}")

        # print("\nUnwanted keywords are : ")
        # for c_unwanted_keyword in list_unwanted_keywords : 
        #     print(f"- {c_unwanted_keyword}")


        with open(follow_process_path, "a") as info_file:
            # Informations on the srcipt :
            info_file.write("\n# Execution's informations (time, memory):\n")
            info_file.write(f"\nSearching if transcription factors are annotated by go terms of interest taken **{round(((time).time() - t0), 3)} seconds <=> {round((((time).time() - t0)/60), 3)} minutes**.\n")



    # Stores process informations : 

    # statistique informations on data  :
    text_info = """

 # Data analysis and statistics :
 
 
 ## Statistics on numerics TF table datas :

"""

    if strict_MeSH_terms : 
        # makes 7 intervals (to not have to much lines) of quotations counts, but tu gather TF as a list with their nb : 


        pipeline.stat(list_TF[["strict_quotations_nb"]], "strict_quotations_nb", "hist, box", "\t", f"{follow_process_dir}/graphics")

        text_info += f"""


## strict_quotations_nb : 
***

{pipeline.display_table_stat("graphics/strict_quotations_nb_hist.png", "strict_quotations_nb_hist", "graphics/strict_quotations_nb_box.png" , "strict_quotations_nb_box", pipeline.makes_percentiles_intervals(list_TF[["Transcription_Factor", "strict_quotations_nb"]], "strict_quotations_nb").to_markdown())}

***
"""
# {list_TF[["strict_quotations_nb"]].astype(float).describe().to_markdown()}

# ***

# """
        
    if large_MeSH_terms : 
        pipeline.stat(list_TF[["large_quotations_nb"]], "large_quotations_nb", "hist, box", "\t", f"{follow_process_dir}/graphics")

        text_info += f"""


## large_quotations_nb : 
***

{pipeline.display_table_stat("graphics/large_quotations_nb_hist.png", "large_quotations_nb_hist", "graphics/large_quotations_nb_box.png" , "large_quotations_nb_box", pipeline.makes_percentiles_intervals(list_TF[["Transcription_Factor", "large_quotations_nb"]], "large_quotations_nb").to_markdown())}

***
"""
# {list_TF[["large_quotations_nb"]].astype(float).describe().to_markdown()}

# ***
# """

    if get_G_quotations_nb : 
        pipeline.stat(list_TF[["G_quotations_nb"]], "G_quotations_nb", "hist, box", "\t", f"{follow_process_dir}/graphics")

        text_info += f"""

## G_quotations_nb : 
***

{pipeline.display_table_stat("graphics/G_quotations_nb_hist.png", "G_quotations_nb_hist", "graphics/G_quotations_nb_box.png" , "G_quotations_nb_box", pipeline.makes_percentiles_intervals(list_TF[["Transcription_Factor", "G_quotations_nb"]], "G_quotations_nb").to_markdown())}

***
"""
# {list_TF[["G_quotations_nb"]].astype(float).describe().to_markdown()}

# ***
# """

    # Calculates how many TF's are annotated by go terms of interest : 
    if list_go_interest :
        df_count_TF_go = {}
        for go_interest in list_go_interest :
            if not already_annotation_table : 
                df_count_TF_go[go_interest[:-8]] = [list(list_TF[go_interest[:-8]]).count("X"), ", ".join(list_TF[list_TF[go_interest[:-8]] == "X"]["Transcription_Factor"])]
            else :
                df_count_TF_go[go_interest] = [list(list_TF[go_interest]).count("X"), ", ".join(list_TF[list_TF[go_interest] == "X"]["Transcription_Factor"])]

        go_terms_count_stat = pd.DataFrame({"go_terms" : list(df_count_TF_go.keys()), "count_per_go_terms" : [value[0] for value in list(df_count_TF_go.values())], "TF" : [value[1] for value in list(df_count_TF_go.values())]})

        go_terms_count_stat.plot(kind = "bar", x = "go_terms", y = "count_per_go_terms", figsize = (10,12), legend = True, title = "count_per_go_terms")
        plt.savefig(f"{follow_process_dir}/graphics/count_per_go_terms_bar.png")
        plt.close()

        text_info += f"""

## Table of number of TF with annotation and their labels :
***

<table>
    <thead>
        <tr>
            <th>graphic</th>
            <th>table</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=4><img src=graphics/count_per_go_terms_bar.png alt=graĥic width="500"/></td>
            <td rowspan=2>

{go_terms_count_stat.sort_values(by = "count_per_go_terms", ascending = False).to_markdown()}

</td>
        </tr>
        <tr>
        </tr>
    </tbody>
</table>

***
"""

    # Counts how many TF are annotated by a given keyworks and which ones : 
    if list_categories_keywords :
        for c_category in list_categories_keywords : 
            category_table = {}
            for keywords, TF in zip(list_TF[c_category], list_TF["Transcription_Factor"]) :
                if str(keywords) == "nan" :
                    continue
                keywords = keywords.split(", ")
                for c_keyword in keywords : 
                    if c_keyword in category_table.keys() :
                        category_table[c_keyword][0] += 1
                        category_table[c_keyword][1] += ", " + TF
                    else : 
                        category_table[c_keyword] = [1, TF]

            category_table = pd.DataFrame({"keywords" : list(category_table.keys()), "keywords_count" : [value[0] for value in list(category_table.values())], "TF" : [value[1] for value in list(category_table.values())]})

            if not all([str(keyword) != "Na" for keyword in category_table["keywords"]]) :

                pipeline.stat(category_table[["keywords", "keywords_count"]], "keywords_count", "hist, box", "\t", f"{follow_process_dir}/graphics")

                category_table.plot(kind = "bar", x = "keywords", y = "keywords_count", figsize = (10,12), legend = True, title = "keywords_count")
                plt.savefig(f"{follow_process_dir}/graphics/keywords_count_bar.png")
                plt.close()

                text_info += f"""

## Keywords count on category {c_category} :
***

<table>
    <thead>
        <tr>
            <th>graphic</th>
            <th>table</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=4><img src=graphics/keywords_count_bar.png alt=graĥic width="500"/></td>
            <td rowspan=2>

{category_table.sort_values(by = "keywords_count", ascending = False).to_markdown()}

</td>
        </tr>
        <tr>
        </tr>
    </tbody>
</table>

***
"""


    print("\nFirst lines of the TF's annotated table are : ")
    print(list_TF.head())
    
    list_all_TF = list_all_TF.merge(list_TF, how = "left")
    list_TF = []

    # Cheks if directory exists 
    if not os.path.exists("/".join(path_TF_table_annotated.split("/")[:-1])):
        os.makedirs("/".join(path_TF_table_annotated.split("/")[:-1]))

    list_all_TF.to_csv(path_TF_table_annotated, sep = sep_table, index = None)

    if already_annotation_table : 
        path_annotated_table = already_annotation_table  # Path to all TF annotated table 

    else : 
        # stores columns index for columns index labels in a dictionnary : 
        dict_annot_columns_id = {}
        if get_G_quotations_nb : 
            dict_annot_columns_id["column_G_quotations"] = list(list_all_TF.columns).index("G_quotations_nb")
        else : 
            dict_annot_columns_id["column_G_quotations"] = None

        if strict_MeSH_terms :    
            dict_annot_columns_id["column_strict_MeSH_quotations"] = list(list_all_TF.columns).index("strict_quotations_nb")
        else : 
            dict_annot_columns_id["column_strict_MeSH_quotations"] = None

        if large_MeSH_terms : 
            dict_annot_columns_id["column_large_MeSH_quotations"] = list(list_all_TF.columns).index("large_quotations_nb")
        else : 
            dict_annot_columns_id["column_large_MeSH_quotations"] = None 

        if list_categories_keywords :
            dict_annot_columns_id["columns_keywords_categories"] = []
            for c_category in list_categories_keywords :
                dict_annot_columns_id["columns_keywords_categories"].append(list(list_all_TF.columns).index(c_category))
        else : 
            dict_annot_columns_id["columns_keywords_categories"] = None

        if list_go_interest :
            dict_annot_columns_id["columns_go_terms"] = []
            for c_go in list_go_interest : 
                dict_annot_columns_id["columns_go_terms"].append(list(list_all_TF.columns).index(c_go[:-8]))
        else : 
            dict_annot_columns_id["columns_go_terms"] = None

        path_annotated_table = path_TF_table_annotated

    text_info += f"""

***

### dict_annot_columns_id : (if the annotation table will be reused in an other query)


{dict_annot_columns_id}


### Columns labels :

{list_all_TF.columns}

""" 

    # Execution time 
    print(f"Ends at {round((time.time() - t0), 3)} seconds <=> {round(((time.time() - t0)/60), 3)} minutes")

    with open(follow_process_path, "a") as follow_process_file :
        follow_process_file.write(text_info)
        follow_process_file.write(f"\n\nExecution time : {round((time.time() - t0), 3)} seconds <=> {round(((time.time() - t0)/60), 3)} minutes\n")

    # execution_time (sec), execution_time (min), memory_usage (MiB), input_file_size (MiB)
    return [dict_annot_columns_id, path_annotated_table, [round((time.time() - t0), 3), round(((time.time() - t0)/60), 3), int(getrusage(RUSAGE_SELF).ru_maxrss / 1024), int(input_size / 1024)]]


if __name__ == "__main__" : 

    parser = argparse.ArgumentParser()

    parser.add_argument("-tf", "--list_TF", help = "Path to the transcription factor's list selected by Regulus.")
    parser.add_argument("-s", "--sep_table", help = "String that separates columns in transcription table of interest.")
    parser.add_argument("--email", help = "email address to informs who launch query to nbci (mandatory).")
    parser.add_argument("-go", "--list_go_interest", help = "String of go terms of interest separated by ','. Example : 'cellular developmental process:0048869, immune system process:0002376, B cell activation:0042113, plasma cell differenciation:0002317, B cell mediated immunity:0019724'")
    parser.add_argument("-kw", "--list_unwanted_keywords", help = "Specific keywords that are not wanted separated by ', 'example : 'DNA-binding, Repressor, Activator, Transcription regulation'.")
    parser.add_argument("-ckw", "--list_categories_keywords", help = "Wanted categories' keywords (as : Molecular function, Biological Process...) ")
    parser.add_argument("-aan", "--already_annotation_table", help = "Path to the all transcription factor annotated table.")
    parser.add_argument("--dict_annot_columns_id", help = "Dictionnary of annotations columns number given by an other query (in the follow process file) to reuse annotations of a table.")
    parser.add_argument("-strict_quote", "--strict_MeSH_terms", help = "Maximum list of MeSH terms (labels) that papers must be annotated with to be counted.")
    parser.add_argument("-large_quote", "--large_MeSH_terms", help = "Maximum list of MeSH terms (labels) that papers must be annotated with to be counted.")
    parser.add_argument("-quote", "--get_G_quotations_nb", help = "  Bool, if general papers' quotation for this TF number must be return as a column.", action = "store_true", default = True)
    parser.add_argument("--follow_process_path", help = "Path to the file to save process data.")
    parser.add_argument("--path_TF_table_annotated", help = "Path to transcription factors annotated file.")

    # add one for the SPARQL endoint ?

    args = parser.parse_args()

    get_annot_tf(args.email, args.path_TF_table_annotated, args.follow_process_path, args.list_TF, args.sep_table, args.list_go_interest, args.list_unwanted_keywords, args.list_categories_keywords, args.annotation_table, args.dict_annot_columns_id, args.strict_MeSH_terms, args.large_MeSH_terms, args.get_G_quotations_nb)