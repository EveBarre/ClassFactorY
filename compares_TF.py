#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
@author : Ève Barré

This scripts compares transcription factors' lists and returns TF that are specific and common from tables.
Theres two tables must have a "Transcription Factor" column label. 

--> Arguments : 

    "-t1", "--TF_table_1" : Path to the first file containing Transcription factors to compare (csv).
    "-t2", "--TF_table_2" : Path to the second file containing Transcription factors to compare (csv).
    "-n", "--comparison_name" : Name to given to the comparison, example : (threshold q0.5 vs q0.75).
    "-o", "--output_file" : Path to output file containing comparison (markdown or txt).
    "--follow_process_path" : Path to the file to save process data. (markdown)

--> Proceeding : 
- imports both tables to compare (t1, t2)
- defines commons transcription factors : labels and count
- defines specific transcription factors in each tables : labels and count
- stores this comparison table to the name given (n) in the file given (o)
- writes in a file (follow_process_path) following process data 

--> In the pipeline : 
Compares_TF can be launch to compare two differents TFs output in order to 
defines which parameters are better to use.

--> Execution time and memory evolution : 
Execution time depends on : 
    - TFs tables size

--> Usage tips : 
Use it to select genes count threshold for instance,
to see which TF are lost from a specific threshold. 

'''

import pandas as pd
import argparse 
import time 
import datetime 
import os
# import nbformat
from resource import getrusage, RUSAGE_SELF  # to assess the peak memory usage


def compares_TF(follow_process_path, TF_table_1, TF_table_2, comparison_name, output_file, sep_table) : 
    '''
    '''

    t0 = time.time()

    # Checks existence of input files : 
    if not os.path.exists(TF_table_1) :
        raise ValueError(f"{TF_table_1} does not exist. Change input.")
    if not os.path.exists(TF_table_2) :
        raise ValueError(f"{TF_table_2} does not exist. Change input.")
    
   # asserts is a .md file 
    if follow_process_path[-3:] != ".md" :
        raise ValueError(f"'follow_process_path' must be a markdown (.md) file not {follow_process_path.split('.')[-1]}.")

    # markdown file to follow process :
    text_info = (f"""

# <font color=orange>Module compares_TF.py :</font> 

lauched at {datetime.datetime.today()}

with arguments :

| Arguments | |
| --- | --- |
| follow_process_path | {follow_process_path} |
| TF_table_1 | {TF_table_1} |
| TF_table_2 | {TF_table_2} |
| comparison_name | {comparison_name} |
| output_file | {output_file} |


""")
    print(f"""

********************************************************************************
--> Module compares_TF.py :
lauched at {datetime.datetime.today()}
********************************************************************************

""")

    if not os.path.exists("/".join(follow_process_path.split("/")[:-1])):
        os.makedirs("/".join(follow_process_path.split("/")[:-1]))

    with open(follow_process_path, "a") as info_file:
        # Informations on the srcipt :
        info_file.write(text_info)

    # Imports data : 
    table_1 = pd.read_csv(TF_table_1, sep = sep_table, header = 0, usecols = ["Transcription_Factor"], engine = "python").drop_duplicates()
    table_2 = pd.read_csv(TF_table_2, sep = sep_table, header = 0, usecols = ["Transcription_Factor"], engine = "python").drop_duplicates()

    table_compare = pd.DataFrame({})
    common_TF = table_1.merge(table_2, on = ["Transcription_Factor"], how = "inner")
    table_compare[f"common_TFs_{common_TF.shape[0]}"] = [(", ").join(common_TF["Transcription_Factor"])]


    if not os.path.exists("/".join(output_file.split("/")[:-1])):
        os.makedirs("/".join(output_file.split("/")[:-1]))


    with open(output_file, "a") as file_result :
        file_result.write(f"\n\n# {comparison_name}\n\n")

        list_spe_TF = []
        for TF in table_1["Transcription_Factor"] :
            if TF not in list(common_TF["Transcription_Factor"]) :
                list_spe_TF.append(TF)
        table_compare[f"1_all_{table_1.shape[0]}_spe_{len(list_spe_TF)}"] = [", ".join(list_spe_TF)]

        list_spe_TF = []
        for TF in table_2["Transcription_Factor"] :
            if TF not in list(common_TF["Transcription_Factor"]) :
                list_spe_TF.append(TF)

        table_compare[f"2_all_{table_2.shape[0]}_spe_{len(list_spe_TF)}"] = [", ".join(list_spe_TF)]

        table_compare.to_markdown(file_result)

    # Execution time 
    print(f"Ends at {round((time.time() - t0), 3)} seconds <=> {round(((time.time() - t0)/60), 3)} minutes")

    with open(follow_process_path, "a") as info_file:
        # Informations on the srcipt :
        info_file.write(f"\nExecution time : {round((time.time() - t0), 3)} seconds <=> {round(((time.time() - t0)/60), 3)} minutes")
    
    # execution_time (sec), execution_time (min), memory_usage (MiB), input_file_size (MiB)
    return [round((time.time() - t0), 3), round(((time.time() - t0)/60), 3), int(getrusage(RUSAGE_SELF).ru_maxrss / 1024), int((os.path.getsize(TF_table_1) + os.path.getsize(TF_table_1)) / 1024)]



if __name__ == "__main__" : 

    parser = argparse.ArgumentParser()

    parser.add_argument("-t1", "--TF_table_1", help = "Path to the first file containing Transcription factors to compare.")
    parser.add_argument("-t2", "--TF_table_2", help = "Path to the second file containing Transcription factors to compare.")
    parser.add_argument("-n", "--comparison_name", help = "Name to given to the comparison.")
    parser.add_argument("-o", "--output_file", help = "Path to output file containing comparison.")
    parser.add_argument("--follow_process_path", help = "Path to the file to save process data.")
    parser.add_argument("--sep_table", help = "String that separates tables.")

    args = parser.parse_args()

    compares_TF(args.follow_process_path, args.TF_table_1, args.TF_table_2, args.comparison_name, args.output_file, args.sep_table)

    # selection method : 
    # python3 compares_TF.py -t1 "/home/eveadmin/Bureau/Class_Factory/TF_tables/693_TF.csv" -t2 "/home/eveadmin/Bureau/Class_Factory/TF_tables/all_TF_ini.csv" -n in_Regulus_vs_expressed  -o "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_selection_threshold_method/TF_comparison.md" --follow_process_path "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_selection_threshold_method/TF_comparison_account.md" --sep_table "\t"; python3 compares_TF.py -t1 "/home/eveadmin/Bureau/Class_Factory/TF_tables/all_TF_ini.csv" -t2 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_selection_threshold_method/test_no_no_q25/TF_tables/selected_TF.csv" -n expressed_vs_q25  -o "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_selection_threshold_method/TF_comparison.md" --follow_process_path "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_selection_threshold_method/TF_comparison_account.md" --sep_table "\t"; python3 compares_TF.py -t1 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_selection_threshold_method/test_no_no_q25/TF_tables/selected_TF.csv" -t2 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_selection_threshold_method/test_no_no_q50/TF_tables/selected_TF.csv" -n q25_vs_q50  -o "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_selection_threshold_method/TF_comparison.md" --follow_process_path "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_selection_threshold_method/TF_comparison_account.md" --sep_table "\t"; python3 compares_TF.py -t1 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_selection_threshold_method/test_no_no_q50/TF_tables/selected_TF.csv" -t2 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_selection_threshold_method/test_no_no_q75/TF_tables/selected_TF.csv" -n q50_vs_q75  -o "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_selection_threshold_method/TF_comparison.md" --follow_process_path "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_selection_threshold_method/TF_comparison_account.md" --sep_table "\t"; python3 compares_TF.py -t1 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_selection_threshold_method/test_no_no_q75/TF_tables/selected_TF.csv" -t2 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_selection_threshold_method/test_no_no_q85/TF_tables/selected_TF.csv" -n q75_vs_q85  -o "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_selection_threshold_method/TF_comparison.md" --follow_process_path "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_selection_threshold_method/TF_comparison_account.md" --sep_table "\t"; python3 compares_TF.py -t1 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_selection_threshold_method/test_no_no_q85/TF_tables/selected_TF.csv" -t2 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_selection_threshold_method/test_no_no_q95/TF_tables/selected_TF.csv" -n q85_vs_q95  -o "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_selection_threshold_method/TF_comparison.md" --follow_process_path "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_selection_threshold_method/TF_comparison_account.md" --sep_table "\t";
    # del : 
    # python3 compares_TF.py -t1 "/home/eveadmin/Bureau/Class_Factory/TF_tables/693_TF.csv" -t2 "/home/eveadmin/Bureau/Class_Factory/TF_tables/all_TF_ini.csv" -n in_Regulus_vs_diff_expressed  -o "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/TF_comparison.md" --follow_process_path "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/TF_comparison_account.md" --sep_table "\t"; python3 compares_TF.py -t1 "/home/eveadmin/Bureau/Class_Factory/TF_tables/all_TF_ini.csv" -t2 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_no_no/TF_tables/selected_TF.csv" -n diff_expressed_vs_no_q75  -o "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/TF_comparison.md" --follow_process_path "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/TF_comparison_account.md" --sep_table "\t"; python3 compares_TF.py -t1 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_no_no/TF_tables/selected_TF.csv" -t2 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/test_no_3/TF_tables/selected_TF.csv" -n no_vs_3  -o "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/TF_comparison.md" --follow_process_path "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/TF_comparison_account.md" --sep_table "\t"; python3 compares_TF.py -t1 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/test_no_3/TF_tables/selected_TF.csv" -t2 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/test_no_6/TF_tables/selected_TF.csv" -n 3_vs_6  -o "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/TF_comparison.md" --follow_process_path "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/TF_comparison_account.md" --sep_table "\t"; python3 compares_TF.py -t1 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/test_no_6/TF_tables/selected_TF.csv" -t2 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/test_no_10/TF_tables/selected_TF.csv" -n 6_vs_10  -o "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/TF_comparison.md" --follow_process_path "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/TF_comparison_account.md" --sep_table "\t"; python3 compares_TF.py -t1 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/test_no_10/TF_tables/selected_TF.csv" -t2 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/test_no_15/TF_tables/selected_TF.csv" -n 10_vs_15  -o "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/TF_comparison.md" --follow_process_path "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/TF_comparison_account.md" --sep_table "\t"; python3 compares_TF.py -t1 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/test_no_15/TF_tables/selected_TF.csv" -t2 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/test_no_20/TF_tables/selected_TF.csv" -n 15_vs_20  -o "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/TF_comparison.md" --follow_process_path "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/TF_comparison_account.md" --sep_table "\t"; python3 compares_TF.py -t1 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/test_no_20/TF_tables/selected_TF.csv" -t2 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/test_no_35/TF_tables/selected_TF.csv" -n 20_vs_35  -o "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/TF_comparison.md" --follow_process_path "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/TF_comparison_account.md" --sep_table "\t"; python3 compares_TF.py -t1 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/test_no_35/TF_tables/selected_TF.csv" -t2 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/test_no_50/TF_tables/selected_TF.csv" -n 35_vs_50  -o "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/TF_comparison.md" --follow_process_path "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/TF_comparison_account.md" --sep_table "\t"; python3 compares_TF.py -t1 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/test_no_50/TF_tables/selected_TF.csv" -t2 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/test_no_150/TF_tables/selected_TF.csv" -n 50_vs_150  -o "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/TF_comparison.md" --follow_process_path "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/TF_comparison_account.md" --sep_table "\t"; python3 compares_TF.py -t1 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/test_no_150/TF_tables/selected_TF.csv" -t2 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/test_no_350/TF_tables/selected_TF.csv" -n 150_vs_350  -o "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/TF_comparison.md" --follow_process_path "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/TF_comparison_account.md" --sep_table "\t"; python3 compares_TF.py -t1 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/test_no_350/TF_tables/selected_TF.csv" -t2 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/test_no_500/TF_tables/selected_TF.csv" -n 350_vs_500  -o "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/TF_comparison.md" --follow_process_path "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/TF_comparison_account.md" --sep_table "\t"; python3 compares_TF.py -t1 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/test_no_500/TF_tables/selected_TF.csv" -t2 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/test_no_600/TF_tables/selected_TF.csv" -n 500_vs_600  -o "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/TF_comparison.md" --follow_process_path "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/TF_comparison_account.md" --sep_table "\t"; python3 compares_TF.py -t1 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/test_no_600/TF_tables/selected_TF.csv" -t2 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/test_no_800/TF_tables/selected_TF.csv" -n 600_vs_800  -o "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/TF_comparison.md" --follow_process_path "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/TF_comparison_account.md" --sep_table "\t"; python3 compares_TF.py -t1 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/test_no_800/TF_tables/selected_TF.csv" -t2 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/test_no_1000/TF_tables/selected_TF.csv" -n 800_vs_1000  -o "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/TF_comparison.md" --follow_process_path "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_del_threshold/TF_comparison_account.md" --sep_table "\t";
    # gather : 
    # python3 compares_TF.py -t1 "/home/eveadmin/Bureau/Class_Factory/TF_tables/693_TF.csv" -t2 "/home/eveadmin/Bureau/Class_Factory/TF_tables/all_TF_ini.csv" -n in_Regulus_vs_diff_expressed  -o "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/TF_comparison.md" --follow_process_path "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/TF_comparison_account.md" --sep_table "\t"; python3 compares_TF.py -t1 "/home/eveadmin/Bureau/Class_Factory/TF_tables/all_TF_ini.csv" -t2 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_no_no/TF_tables/selected_table.csv" -n diff_expressed_vs_no_q75  -o "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/TF_comparison.md" --follow_process_path "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/TF_comparison_account.md" --sep_table "\t"; python3 compares_TF.py -t1 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/test_no_no/TF_tables/selected_table.csv" -t2 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/test_3_no/TF_tables/selected_TF.csv" -n no_vs_3  -o "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/TF_comparison.md" --follow_process_path "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/TF_comparison_account.md" --sep_table "\t"; python3 compares_TF.py -t1 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/test_3_no/TF_tables/selected_TF.csv" -t2 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/test_6_no/TF_tables/selected_TF.csv" -n 3_vs_6  -o "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/TF_comparison.md" --follow_process_path "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/TF_comparison_account.md" --sep_table "\t"; python3 compares_TF.py -t1 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/test_6_no/TF_tables/selected_TF.csv" -t2 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/test_10_no/TF_tables/selected_TF.csv" -n 6_vs_10  -o "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/TF_comparison.md" --follow_process_path "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/TF_comparison_account.md" --sep_table "\t"; python3 compares_TF.py -t1 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/test_10_no/TF_tables/selected_TF.csv" -t2 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/test_15_no/TF_tables/selected_TF.csv" -n 10_vs_15  -o "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/TF_comparison.md" --follow_process_path "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/TF_comparison_account.md" --sep_table "\t"; python3 compares_TF.py -t1 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/test_15_no/TF_tables/selected_TF.csv" -t2 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/test_20_no/TF_tables/selected_TF.csv" -n 15_vs_20  -o "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/TF_comparison.md" --follow_process_path "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/TF_comparison_account.md" --sep_table "\t"; python3 compares_TF.py -t1 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/test_20_no/TF_tables/selected_TF.csv" -t2 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/test_35_no/TF_tables/selected_TF.csv" -n 20_vs_35  -o "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/TF_comparison.md" --follow_process_path "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/TF_comparison_account.md" --sep_table "\t"; python3 compares_TF.py -t1 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/test_35_no/TF_tables/selected_TF.csv" -t2 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/test_50_no/TF_tables/selected_TF.csv" -n 35_vs_50  -o "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/TF_comparison.md" --follow_process_path "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/TF_comparison_account.md" --sep_table "\t"; python3 compares_TF.py -t1 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/test_50_no/TF_tables/selected_TF.csv" -t2 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/test_150_no/TF_tables/selected_TF.csv" -n 50_vs_150  -o "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/TF_comparison.md" --follow_process_path "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/TF_comparison_account.md" --sep_table "\t"; python3 compares_TF.py -t1 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/test_150_no/TF_tables/selected_TF.csv" -t2 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/test_350_no/TF_tables/selected_TF.csv" -n 150_vs_350  -o "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/TF_comparison.md" --follow_process_path "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/TF_comparison_account.md" --sep_table "\t"; python3 compares_TF.py -t1 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/test_350_no/TF_tables/selected_TF.csv" -t2 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/test_500_no/TF_tables/selected_TF.csv" -n 350_vs_500  -o "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/TF_comparison.md" --follow_process_path "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/TF_comparison_account.md" --sep_table "\t"; python3 compares_TF.py -t1 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/test_500_no/TF_tables/selected_TF.csv" -t2 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/test_800_no/TF_tables/selected_TF.csv" -n 500_vs_800  -o "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/TF_comparison.md" --follow_process_path "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/TF_comparison_account.md" --sep_table "\t"; python3 compares_TF.py -t1 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/test_800_no/TF_tables/selected_TF.csv" -t2 "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/test_1000_no/TF_tables/selected_TF.csv" -n 800_vs_1000  -o "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/TF_comparison.md" --follow_process_path "/home/eveadmin/Bureau/Class_Factory/scripts_tests_brouillons/test_healthy_B/tests_gene_gene_pattern_to_gather_threshold/TF_comparison_account.md" --sep_table "\t";
