### relations_table_analysis :



#### General description : 

Relations_table_analysis_1 provides basic statistics about input data. 
For example it indicates how many genes and transcription factors (TF) compose the dataset, 
the ditribution of genes count per expression pattern, etc... 


#### Arguments :

| Arguments | Explanations | Examples |
| --- | --- | --- |
| Relations_table | Path to the relation (TF-gene) table, mus have following columns label ('Transcription_Factor', 'Transcription_Factor', 'Gene', 'Pattern_Gene', 'Pattern_TF', 'Reg'). | work_directory/Relations_tables/my_Relations_table.csv |
| genes_per_gene_pattern_count_table | Path to the file containing the count of genes per gene's pattern, with columns labels 'Pattern_Gene', 'Nb_genes_per_gene_pattern' (.csv). | work_directory/genes_per_gene_pattern_count_tables/genes_per_gene_pattern_count_table.csv |
| follow_process_path | Path to file storing following process data (.md) | /home/user/workdirectory/Account.md |
| sep_table | String that separates columns in tables | '\t' |

![relations_table_analysis_diagram](relations_table_analysis_diagram.png)
