### get_annot_tf : 


Get_annot_tf provides several TF annotations in order to highlight known TF in the biological context in question. 
Possible annotations are : 
- a) if the TF is associated with Gene Ontology (GO) terms of interest (graph_go helps the choice);
- b) the count of TF's quotations in pubmed database papers in tree possible conditions : 
    - The first count depict all TF citation whatever the paper, 
    - the second count one defines that articles must be annotated by a given general combinaison of keywords (Medical Subject Headings MeSH terms), 
    - and a more stringent stringent one using logical operators AND and OR too (see get_MeSH to choice);
- c) keywords associated to TF in uniprot database (https://sparql.uniprot.org/sparql/), in order to visualize if TF is not directly annotated by GO terms of interest, 
in which biological process, disease, molecular function for example (ten possible user_adjustable keywords categories) it is known.
The serach of annotation can be done in the first part, before any TF selection or after (get_annot_tf_2). 

Comment : This module require stable internet connection in order to query databases.


#### Arguments : 

| Arguments | Explanations | Examples |
| --- | --- | --- |
| Relations_table | Path to the relation (TF-gene) table, mus have following columns label ('Transcription_Factor', 'Transcription_Factor', 'Gene', 'Pattern_Gene', 'Pattern_TF', 'Reg'). |  work_directory/Relations_tables/my_Relations_table.csv |
| genes_per_gene_pattern_count_table | Path to the file containing the count of genes per gene's pattern, with columns labels 'Pattern_Gene', 'Nb_genes_per_gene_pattern' (.csv). | work_directory/genes_per_gene_pattern_count_tables/genes_per_gene_pattern_count_table.csv |
| follow_process_path | Path to file storing following process data (.md) | /home/user/workdirectory/Account.md |
| sep_table | String that separates columns in tables | '\t' |
| email | email address to informs who launch query to nbci (mandatory). | firstname.name@example.fr |
| go_interest | String of go terms (label:id) of interest | transcription regulator activity:0140110, cellular developmental process:0048869, immune system process:0002376, lymphocyte activation:0046649, B cell activation:0042113, plasma cell differenciation:0002317, B cell differentiation:0030183 |
| unwanted_keywords | Specific keywords that are not wanted  | 'DNA-binding, Repressor, Activator, Transcription regulation' | 
| strict_MeSH_combinaition| Maximum list of MeSH terms (labels) that papers must be annotated with to be counted, (choice help with get_MeSH module) | (Plasma Cells OR B-Lymphocytes OR Lymphocyte Activation OR Germinal Center) AND (Transcription Factors OR Gene Expression Regulation) AND Cell Differentiation | 
| large_MeSH_combinaition | Maximum list of MeSH terms (labels) that papers must be annotated with to be counted, (choice help with get_MeSH module) | (B-Lymphocytes OR Plasma Cells) | 
| get_G_quotations_nb | Bool, if general papers' quotation for this TF number must be return as a column. | X |
| already_annotation_table | Path to the all transcription factor annotated table. | work_directory/TF_tables/my_all_TF_annotated_table.csv |
| dict_annotations_columns_id | Dictionnary of annotations columns number given by an other query (in the follow process file) to reuse annotations of a table. | {'column_G_quotations': 2, 'column_strict_MeSH_quotations': 5, 'column_large_MeSH_quotations': 3, 'columns_keywords_categories': [14], 'columns_go_terms': [7, 8, 9, 10, 11, 12, 13]} |
| all_TF_table_annotated or <br>TF_table_annotated_file | Stores file's name's transcription factors annotated. | work_directory/TF_tables/my_TF_list_annotated.csv |
| categories_keywords | Keywords category to allow to be display as a column (Bioligocal process, molecular dunction, cellular component, coding sequence diversity, developmental stage, disease, domain, ligand, post_translational modification, technical term)| X (bool for each categories) |


![get_annot_tf_diagram](get_annot_tf_diagram.png)
