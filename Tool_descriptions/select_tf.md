### select_tf : 


#### General description :

The TF's selection is done on its coverage and specificity for a gene pattern. 
Coverage depict which part of the gene pattern is regulated by a specific TF. 
Specificity represents the number of targets of a TF which belong to a specific pattern i.e., 
if a TF preferentially regulates one gene expression pattern over the others. 
Select_tf produces specificity and coverage calculations. 
Once computed, percentages are used to compare TF and select major relations. 
A threshold can be set as mean + one standard deviation, a quantile and/or a specific percentage. 


#### Arguments : 

| Arguments |Explanations | Examples |
| --- | --- | --- |
| Relations_table | Path to the relation (TF-gene) table, mus have following columns label ('Transcription_Factor', 'Transcription_Factor', 'Gene', 'Pattern_Gene', 'Pattern_TF', 'Reg'). | work_directory/Relations_tables/my_Relations_table.csv |
| genes_per_gene_pattern_count_table | Path to the file containing the count of genes per gene's pattern, with columns labels 'Pattern_Gene', 'Nb_genes_per_gene_pattern' (.csv). | work_directory/genes_per_gene_pattern_count_tables/genes_per_gene_pattern_count_table.csv |
| follow_process_path | Path to file storing following process data (.md) | /home/user/workdirectory/Account.md |
| sep_table | String that separates columns in tables | '\t' |
| store_relation_table_file | Path to the selected reltations' table file, if wanted to be returned (.csv). | work_directory/Relations_tables/selected_relations.csv | 
| stores_table_TF_file | Path to the selected transcription factors file, if wanted to be returned (.csv). | work_directory/TF_tables/selected_TF.csv |
| threshold_type | The type of threshold | mean+std (quantile : q50, q75...) | 
| percentage_threshold_cov_spe | Minimum percentage of TF's coverage and specificy for a gene_pattern in decimal to filter | '0.75,0.80' (or '0.75,No' meaning 'cov,spe') | 
| preselected_table | Path to  the preselected table but with data on the selection (.csv). | work_directory/Relations_tables/preselected_relations.csv | 
| columns to conserve | Specifies columns in the output table to concerve (example for all columns : 'Transcription_Factor', 'Pattern_TF', 'Nb_genes_per_TF', 'Nb_relations_selected', 'selected_pattern_nb', 'Patterns_targeted_pos', 'Patterns_targeted_neg', 'Patterns_targeted'). | X (bool for each column) |


![select_tf_diagram](select_tf_diagram.png)