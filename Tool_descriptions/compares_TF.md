### compares_TF : 


This scripts compares transcription factors' lists and returns TF that are specific and common from tables.
Theres two tables must have a "Transcription Factor" column label. 

#### Arguments : 

| Arguments | Explanations | Examples |
| --- | --- | --- |
| TF_table_1 | Path to the first file containing Transcription factors to compare (csv). | workdirectory/TF_tables/my_TF_table_1.csv |
| TF_table_2 | Path to the second file containing Transcription factors to compare (csv). | workdirectory/TF_tables/my_TF_table_2.csv |
| follow_process_path | Path to file storing following process data (.md) | /home/user/workdirectory/Account.md |
| sep_table | String that separates columns in tables | '\t' |
| comparison_name | Name to given to the comparison | threshold q50 vs q75 | 
| output_file_compares_TF | Path to output file containing comparison (markdown or txt). | workdirectory/Results_Analysis/TF_comparison.md |


![compares_TF_diagram](compares_TF_diagram.png)
