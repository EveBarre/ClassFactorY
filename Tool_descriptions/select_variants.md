### select_variants : 


Select_variants class variants if they are located on a transcription factor given binding sites (TFBS) on target gene's regulatory regions.
If data are not given, TFBS can be obtained on JASPAR REST API and gene coordinates in Ensembl REST API. 


#### Arguments : 

| Arguments | Explanations | Examples |
| --- | --- | --- |
| reference_genome | Reference genome used and to use to search genome coordinates. | hg19 or hg38 or grch38 or grch37 |
| JASPAR_version | If transcription factor binding sites must be search, precise which version of JASPAR database use. | 2020 | 
| TF_gene_region_file | Relations_table path, can contain or not regions coordinates. | workdirectory/Relations_table/TF_gene_region_file.csv |
| output_variants_file | Path to the file to output (with variants per transcription factor found). | workdirectoey/Results_analysis/TF_variants.csv |
| sep_table | String that separates columns in transcription table of interest. | "\t" |
| VCF_file | Path to the Variant Calling Format file. | workdirectory/variants.vcf |
| regulatory_gene_distance | Distance between a gene and its regulatory regions to consider in bases number. | 500000  (< 2000000) |
| are_regulatory_region_given | Bool, if regions' genomic coordinates are already given in the relation table (start, end, chrom). | X |
| TFBS_file | Path to the transcription binding sites file if already search, otherwise, search on JASPAR. | workdirectory/TFBS.bed |
| follow_process_path | Path to the file to save process data. | workdirectory/Results_analysis/Account.md |
| is_regulatory_direction_given | Bool, if regulatory direction is given as a column in relations table. | X |


![select_variants_diagram](select_variants_diagram.png)
