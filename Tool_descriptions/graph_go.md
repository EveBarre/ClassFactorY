### graph_go : 


#### General description : 

This script provides a hierargical graph of a Gene Ontology term given (parents and children) in order to
help the choice of GO term in get_annot_tf (to know if TFs are annotated by).
GO terms identifiers must be given, a list is possible (separates by a coma), as : 0002317, 0019724.

Comment : This module require stable internet connection in order to query databases.


#### Arguments : 

| Arguments | Explanations | Examples |
| --- | --- | --- |
| follow_process_path | Path to file storing following process data (.md) | /home/user/workdirectory/Account.md |
| go_terms_graph | Label and gene ontology identifer for the one the parent graph is wanted | 0002317, 0019724 |
| graph_go_file | Path to the graph's file. (.png) | work_directory/Results_analysis/graphs/go_graphs/my_go_graph.png |


![graph_go_diagram](graph_go_diagram.png)
