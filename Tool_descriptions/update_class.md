### update_class :


#### General description :

In order to follow TF selection, update_class (launched between each filtering module) classifies TF in a table 
where each line is a filter. It enables to check through which filter a TF of interest is lost for instance.
Filters could be : 
    - all TF
    - differential expression +/- constant
    - compatibility table 
    - specificity + coverage
    - ? : annotations, publication's quotation number ... 

#### Arguments : 


| Arguments | Explanations | Examples |
| --- | --- | --- |
| Relations_table | Path to the relation (TF-gene) table, mus have following columns label ('Transcription_Factor', 'Transcription_Factor', 'Gene', 'Pattern_Gene', 'Pattern_TF', 'Reg'). | work_directory/Relations_tables/my_Relations_table.csv |
| genes_per_gene_pattern_count_table | Path to the file containing the count of genes per gene's pattern, with columns labels 'Pattern_Gene', 'Nb_genes_per_gene_pattern' (.csv). | work_directory/genes_per_gene_pattern_count_tables/genes_per_gene_pattern_count_table.csv |
| follow_process_path | Path to file storing following process data (.md) | /home/user/workdirectory/Account.md |
| sep_table | String that separates columns in tables | '\t' |
| class_table | Path to table that class all TF in function of filters used. | workdirectory/Results_Analysis/class_table.md
| go_terms_as_column | Go terms to keep as column in the classification (it gives the proportion of annotated selected TF).| transcription regulator activity, cellular developmental process, immune system process, lymphocyte activation, B cell activation, plasma cell differenciation, B cell differentiation | 
| quotations_count_as_column | Papers quotation count ('G_quotations_nb', 'strict_quotations_nb', 'large_quotations_nb' to keep as column in the classification (gives the annoated TF count).| X (bool for each) |
| all_TF_annotated_table_path | Path to the annotated transcription factors table. | workdirectory/TF_tables/my_all_annotated_TF.csv |
| filter_name | Filter name applied on transcription factors. (if nothing in part_2) | threshold q75 |
| TF_just_filtered | List of transcription factors just filtered. (if nothing in part_2) | workdirectory/TF_tables/TF_to_filter.csv |
| all_initial_TF | List of transcription factors known initially. (if get_annot_tf_1) | workdirectory/TF_tables/all_initial_TF.csv |


![update_class_diagram](graph_go_diagram.png)

